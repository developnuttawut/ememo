﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eMemo.Controllers
{
    public class MasterSetupController : Controller
    {
   
        public IActionResult MemoMapping()
        {
            return View();
        }
        public IActionResult RuleManagement()
        {
            return View();
        }
        public IActionResult MailManagement()
        {
            return View();
        }
    }
}
