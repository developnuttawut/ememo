﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eMemo.Views.Home
{
    public class PreparationController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        
        public IActionResult Information(string id)
        {
            ViewBag.id = id;
            return View();
        }
    }
}
