﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eMemo.Controllers
{
    public class PrintReportController : Controller
    {
        public IActionResult PrintMemoLetterReport()
        {
            return View();
        }
        public IActionResult DaysAfterMemoPrintedReport()
        {
            return View();
        }
        public IActionResult SendingMemoReport()
        {
            return View();
        }
        public IActionResult PendingReport()
        {
            return View();
        }
        public IActionResult PendingSummaryReport()
        {
            return View();
        }
    }
}
