﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using eMemo.Models;
using Microsoft.AspNetCore.Mvc;

namespace eMemo.Controllers
{
    public class InquiryController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Information(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                InquiryInformation viewModel = new InquiryInformation();
                viewModel.memoTransactionID = Convert.ToInt32(id);
                return View(viewModel);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
}
}
