﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eMemo.Models
{
    public class InquiryViewModel
    {
        public string documentNo { get; set; }
        public string policyNo { get; set; }
        public string proposalNo { get; set; }
        public string ebaoMemoStatusID { get; set; }
        public string osMemo { get; set; }
        public string sending { get; set; }
        public string citizenID { get; set; }
        public string partyID { get; set; }
        public string customerName { get; set; }
        public string customerSurname { get; set; }
        public string agentCode { get; set; }
        public string agentName { get; set; }
        public string agentSurname { get; set; }
        public string agentUnit { get; set; }
        public string agentGroup { get; set; }
        public string transactionDateForm { get; set; }
        public string transactionDateTo { get; set; }
        public string transactionID { get; set; }
        public string policyName { get; set; }
        public string eBaoMemoStatusID { get; set; }
        public string ebaoMemoStatus { get; set; }
    }

    public class InquiryInformation
    {
        public int memoTransactionID { get; set; }
        public basicInformation basicInformation { get; set; }
        public memoDocumentInformation memoDocumentInformation { get; set; }
        public memoAmendmentInformation memoAmendmentInformation { get; set; }
        public List<memoDocumentHistory> memoDocumentHistory { get; set; }
    }
    public class basicInformation
    {
        public string policyNo { get; set; }
        public string proposalNo { get; set; }
        public string proposalDate { get; set; }
        public string policyStatus { get; set; }
        public string policyName { get; set; }
        public string citizenID { get; set; }
        public string mainPlan { get; set; }
        public string agentName { get; set; }
        public string unit { get; set; }
        public string group { get; set; }
        public string agentEmail { get; set; }
        public string agentPhoneNumber { get; set; }
        public string customerEmail { get; set; }
        public string customerPhoneNumber { get; set; }
        public string serviceAgentEmail { get; set; }
        public string serviceAgentPhoneNumber { get; set; }

    }
    public class memoDocumentInformation
    {
        public string ebaoDocumentNo { get; set; }
        public string transactionDate { get; set; }
        public string createdUser { get; set; }
        public string ebaoMemoStatus { get; set; }
        public string ebaoRelpliedUser { get; set; }
        public string ebaoRelpliedDate { get; set; }
    }
    public class memoAmendmentInformation
    {
        public List<document> documents { get; set; }
        public List<history> history { get; set; }
    }

    public class document
    {
        public int memoDetailID { get; set; }
        public int sequence { get; set; }
        public string memoCode { get; set; }
        public string memoDescription { get; set; }
        public string comment { get; set; }
        public string letterStatus { get; set; }
        public DateTime? replyDate { get; set; }
        public string replyBy { get; set; }
        public string source { get; set; }
    }
    public class history
    {
        public string sendTo { get; set; }
        public string description { get; set; }
        public string channel { get; set; }
        public string sendDate { get; set; }
        public string sendTime { get; set; }
        public string sendBy { get; set; }
        public string sendStatus { get; set; }
    }
    public class memoDocumentHistory
    {
        public int memoDocumentHistoryID { get; set; }
        public string documentNo { get; set; }
        public string transactionDate { get; set; }
        public int osMemo { get; set; }
        public string printedBy { get; set; }
    }
    public class status
    {
        public string code { get; set; }
        public string message { get; set; }
    }
}
