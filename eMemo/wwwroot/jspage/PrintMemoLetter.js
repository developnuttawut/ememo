﻿$(document).ready(function() {
    initDatepicker();
    // $('#dtTransactionFrom').datepicker({
    //     language: 'th-th',
    //     format: 'dd/mm/yyyy',
    //     autoclose: true,
    //     defaultDate: new Date()
    // });
    $('#dtTransactionFrom').val(moment().add(543, 'year').format('DD/MM/YYYY'));
    initModalUnit();
    initModalGroup();
    loadDDL();
    checkRole();
    //$('#tblDetails').dataTable().fnClearTable();
    //$('#rowtotal').text("0");
    // $('#dtTransactionFrom').val(moment().format('YYYY-MM-DD'));
    // $('#dtTransactionTo').val(moment().format('YYYY-MM-DD'));

    function checkRole() {
        var menuData = JSON.parse(localStorage.getItem("userData"));
        if (menuData.userItem != null && menuData.userItem.users != null && menuData.userItem.users.applicationMenu != null) {
            var roleName = menuData.userItem.users.roleName;
            if (roleName == "Branch Officer Staff") {
                $('#btnPrintMemoLetterOnly').hide();
            }
        }

    }

    $('#btnResetTable').click(function() {
        $('#rowtotal').text("0");
        $('#tblDetails').dataTable().fnClearTable();
    })

    $('#btnReset').click(function() {
        $('#ddlProductCat').val("");
        $('#ddlDelivery').val("");
        $('#ddleBaoStatus').val("");
        // $('#dtTransactionFrom').val(moment().format('YYYY-MM-DD'));
        // $('#dtTransactionTo').val(moment().format('YYYY-MM-DD'));
        $('#dtTransactionFrom').val("");
        $('#dtTransactionTo').val("");

        $('#txtPolicyNoFrom').val("");
        $('#txtPolicyNoTo').val("");
        $('#txtProposalNoFrom').val("");
        $('#txtProposalNoTo').val("");
        $('#txteBaoDocFrom').val("");
        $('#txteBaoDocTo').val("");

        $('#txtId').val("");
        $('#txtPartyId').val("");
        $('#txtCustomer').val("")
        $('#txtCustomerSurname').val("");

        $('#txtAgentCode').val("");
        $('#txtAgentName').val("");
        $('#txtAgentSurname').val("");
        $('#txtUnit').val("");
        $('#txtGroup').val("");
        $('#rowtotal').text("0");
        $('#tblDetails').dataTable().fnClearTable();
    })

    $('#selectallprintmemo').click(function() {
        var checked = $('#selectallprintmemo').prop('checked');
        $("input:checkbox[name=selectprintmemo]").each(function() {
            $(this).prop("checked", checked);
        });
    });
    $('#btnPrintMemoLetterOnly').click(function() {
        var i = 0;
        let arrayPdf = []
        $("#tblDetails input[type=checkbox]:checked").each(function() {
            i = 1;
            var row = $(this).closest("tr")[0];
            if (row.cells[1].innerHTML.indexOf("Trans") == -1) {
                var documentNo = row.cells[3].innerHTML;
                arrayPdf.push(apiUrl + '/PrintMemoLetter/PrintMemoLetterOnly/' + documentNo)
                // window.open(apiUrl + '/PrintMemoLetter/PrintMemoLetterOnly/' + documentNo, '_blank');
                // $.ajax({
                //     type: 'get',
                //     url: apiUrl + '/PrintMemoLetter/PrintMemoLetterOnly/' + documentNo,
                //     xhrFields: {
                //         responseType: 'blob'
                //     },
                //     success: function (response) {
                //         var link = document.createElement('a');
                //         link.href = window.URL.createObjectURL(response);
                //         link.target = '_blank'
                //         link.click();

                //     }
                // })
            }

        });
        mergeAllPDFs(arrayPdf)
        if (i == 0) {
            alert('โปรดเลือกรายการอย่างน้อย 1 รายการ');
        }
    });

    $('#icoUnit').click(function() {

        $.ajax({
            type: 'get',
            url: apiUrl + '/Transaction/GetAgentUnit',
            success: function(data) {
                $.each(data.data, function(i, vals) {
                    console.log(vals.agentUnitCode);

                    if (vals.agentUnitCode.toLowerCase().indexOf($("#txtUnit").val().toLowerCase()) >= 0) {
                        console.log($("#txtUnit").val());
                        $('#txtUnit').val(vals.agentUnitCode);
                    }
                });

            }
        });
    });

    $('#icoGroup').click(function() {

        $.ajax({
            type: 'get',
            url: apiUrl + '/Transaction/GetAgentGroup',
            success: function(data) {
                $.each(data.data, function(i, vals) {
                    if (vals.agentGroupName.toLowerCase().indexOf($('#txtGroup').val().toLowerCase()) >= 0) {
                        $('#txtGroup').val(vals.agentGroupName);
                    }
                });
            }
        });
    });

    $('#icoCode').click(function() {

        $.ajax({
            type: 'get',
            url: apiUrl + '/Transaction/GetAgentCode',
            success: function(data) {
                $.each(data.data, function(i, vals) {
                    if (vals.GetAgentCode.toLowerCase().indexOf($('#txtAgentCode').val().toLowerCase()) >= 0) {
                        $('#txtAgentCode').val(vals.GetAgentCode);
                    }
                });
            }
        });
    });

    $('#btnPrint').click(function() {
        var i = 0;
        let arrayPdf = []
        $("#tblDetails input[type=checkbox]:checked").each(function() {
            i = 1;
            var row = $(this).closest("tr")[0];
            if (row.cells[1].innerHTML.indexOf("Trans") == -1) {
                var documentNo = row.cells[3].innerHTML;
                arrayPdf.push(apiUrl + '/PrintMemoLetter/Print/' + documentNo)
                // $.ajax({
                //     type: 'get',
                //     url: apiUrl + '/PrintMemoLetter/Print/' + documentNo,
                //     xhrFields: {
                //         responseType: 'blob'
                //     },
                //     success: function (response) {
                //         var link = document.createElement('a');
                //         link.href = window.URL.createObjectURL(response);
                //         link.target = '_blank'
                //         link.click();

                //     }
                // })
            }


        });
        mergeAllPDFs(arrayPdf)
        if (i == 0) {
            alert('โปรดเลือกรายการอย่างน้อย 1 รายการ');
        }
    });

    $('#btnSearch').click(function() {

        LoadDataTable();

    });

    //LoadDataTable();
});

function initModalUnit() {
    var tableUnit = $('#tblUnit').DataTable({
        responsive: true,
        autoWidth: false,
        ajax: {
            url: apiUrl + '/MasterData/GetAgentUnit',
            dataSrc: "data",
        },
        columns: [
            { data: null, defaultContent: '<button class="btn btn-primary btn-xs" type="button">Select</button>', orderable: false },
            { data: 'agentUnitCode', title: 'Unit Code' },
            { data: 'agentUnitName', title: 'Unit Name' }
        ],
    });
    $('#tblUnit tbody').on('click', 'button', function() {
        var data = tableUnit.row($(this).parents('tr')).data();
        $('#txtUnit').val(data.agentUnitName);
        $('#modalUnit').modal('hide');
    });
}

function initModalGroup() {
    var tableGroup = $('#tblGroup').DataTable({
        responsive: true,
        autoWidth: false,
        ajax: {
            url: apiUrl + '/MasterData/GetAgentGroup',
            dataSrc: "data",
        },
        columns: [
            { data: null, defaultContent: '<button class="btn btn-primary btn-xs" type="button">Select</button>', orderable: false },
            { data: 'agentGroupCode', title: 'Group Code' },
            { data: 'agentGroupName', title: 'Group Name' }
        ],
    });
    $('#tblGroup tbody').on('click', 'button', function() {
        var data = tableGroup.row($(this).parents('tr')).data();
        $('#txtGroup').val(data.agentGroupName);
        $('#modalGroup').modal('hide');
    });
}

function LoadDataTable() {
    //  var jsonData = [{
    //      "transactionID": "1",
    //      "transactionDate": "16/06/2563",
    //      "eBaoDocumentNo": "2006000001",
    //      "policyNo": "621000157047",
    //      "proposalNo": "HO 005754920",
    //      "policyName": "นางหฤทัย เดชกุลa",
    //      "agentCode": "L100000049",
    //      "agentName": "นางพัชรา ศรีสกุล",
    //      "agentUnit": "SELIC",
    //      "agentGroup": "SELIC",
    //      "pdfFile": "",
    //      "eBaoMemoStatus": "Printed",
    //      "deliveryStatus": "Successfully"
    //  },
    //  {
    //      "transactionID": "2",
    //      "transactionDate": "16/06/2563",
    //      "eBaoDocumentNo": "2006000001",
    //      "policyNo": "621000157047",
    //      "proposalNo": "HO 005754920",
    //      "policyName": "นางหฤทัย เดชกุลa",
    //      "agentCode": "L100000049",
    //      "agentName": "นางพัชรา ศรีสกุล",
    //      "agentUnit": "SELIC",
    //      "agentGroup": "SELIC",
    //      "pdfFile": "",
    //      "eBaoMemoStatus": "Printed",
    //      "deliveryStatus": "Successfully"
    //  }
    //  ];    
    if (isValidInput()) {


        var param = {
            transactionDateForm: convertThaiToUS($('#dtTransactionFrom').val()),
            transactionDateTo: convertThaiToUS($('#dtTransactionTo').val()),
            productCategory: $("#ddlProductCat").val(),
            fromPolicyNo: $('#txtPolicyNoFrom').val(),
            toPolicyNo: $('#txtPolicyNoTo').val(),
            fromProposalNo: $('#txtProposalNoFrom').val(),
            toProposalNo: $('#txtProposalNoTo').val(),
            fromDocumentNo: $('#txteBaoDocFrom').val(),
            toDocumentNo: $('#txteBaoDocTo').val(),
            ebaoMemoStatusID: $("#ddleBaoStatus").val(),
            sending: $("#ddlDelivery").val(),
            citizenID: $('#txtId').val(),
            partyID: $('#txtPartyId').val(),
            customerName: $('#txtCustomer').val(),
            customerSurname: $('#txtCustomerSurname').val(),
            agentCode: $('#txtAgentCode').val(),
            agentName: $('#txtAgentName').val(),
            agentSurname: $('#txtAgentSurname').val(),
            agentUnit: $('#txtUnit').val(),
            gentGroup: $('#txtGroup').val()
        };
        //const FROM_PATTERN = 'YYYY-MM-DD HH:mm:ss.SSS';
        //const TO_PATTERN = 'DD/MM/YYYY HH:mm';

        $('#tblDetails').dataTable({

            "destroy": true,
            "ajax": {
                type: "POST",
                url: apiUrl + '/PrintMemoLetter/GetPrintMemoLetter',
                data: function(d) {
                    // console.log(JSON.stringify(param));
                    return JSON.stringify(param);
                },
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                //success: function (data) {
                //    $('#rowtotal').text(data.data.length);

                //},

                dataSrc: function(result) {
                    // console.log(result);
                    if (result.status.code == '404') {
                        alert(result.status.message);
                    }
                    $('#rowtotal').text(result.data.length);
                    return result.data;
                }

            },
            //"data": jsonData,
            "columns": [{
                    targets: 0,
                    searchable: false,
                    orderable: false,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        return '<input type="checkbox" name="selectprintmemo" value="' + $('<div/>').text(data).html() + '">';
                    }
                },
                {
                    className: 'hide_column',
                    data: 'transactionID',
                    title: 'Transaction Id'

                },
                {
                    data: 'transactionDate',
                    //render: $.fn.dataTable.render.moment(FROM_PATTERN, TO_PATTERN),
                    title: 'Transaction Date',
                    render: function(data, type, row) {
                        return moment(data, 'DD/MM/YYYY').add(543, 'year').format('DD/MM/YYYY');
                    }
                },
                {
                    data: 'documentNo',
                    title: 'eBao Document No.',
                    // render: function (data, type, row) {
                    //     return '<a href="/eMemo/Inquiry/Information">' + data + '</a>';
                    // },
                },
                { data: 'policyNo', title: 'Policy No.' },
                { data: 'proposalNo', title: 'Proposal No.' },
                { data: 'policyName', title: 'Policy Name' },
                { data: 'agentCode', title: 'Agent Code' },
                { data: 'agentName', title: 'Agent Name' },
                { data: 'agentUnit', title: 'Unit' },
                { data: 'agentGroup', title: 'Group' },
                {
                    data: 'pdfFile',
                    title: 'PDF File',
                    className: 'text-center',
                    render: function(data, type, row) {
                        return '<a href="' + apiUrl + '/FileAttachment/PreviewMemoAttachment/' + row['documentNo'] + '" target="_blank"><ion-icon style="color:red;font-size:25px;cursor:pointer" name="document-attach"></ion-icon></a>';
                    }
                },

                { data: 'ebaoMemoStatus', title: 'eBao Memo Status' },
                { data: 'sending', title: 'Sending' }
            ]
        });

    } else {
        alert('กรุณาเลือกรายการที่ต้องการดำเนินการ');
    }
}

$.fn.dataTable.ext.errMode = 'none';

function loadDDL() {
    $("#ddleBaoStatus").empty();
    $("#ddleBaoStatus").append("<option value=''>All</option>");
    $.ajax({
        url: apiUrl + '/Transaction/GetEBaoStatus',
        success: function(data) {
            $.each(data.data, function(i, val) {
                $("#ddleBaoStatus").append("<option value=" + val.eBaoStatusID + ">" + val.description + "</option>");
            });
        }
    });

    $("#ddlDelivery").empty();
    $("#ddlDelivery").append("<option value=''>All</option>");
    $.ajax({
        url: apiUrl + '/Transaction/GetDeliveryStatus',
        success: function(data) {

            $.each(data.data, function(i, val) {
                $("#ddlDelivery").append("<option value=" + val.deliveryStatusID + ">" + val.description + "</option>");
            });
        }
    });

    $("#ddlProductCat").empty();
    $("#ddlProductCat").append("<option value=''>Please select</option>");
    $.ajax({
        url: apiUrl + '/MasterData/GetProductCategory',
        success: function(data) {
            $.each(data.data, function(i, val) {
                $("#ddlProductCat").append("<option value=" + val.productCategoryID + ">" + val.description + "</option>");
            });
        }
    });
}

function isValidInput() {
    var check = false;
    var arrayInput = [
        $('#ddlProductCat').val(),
        $('#ddlDelivery').val(),
        $('#ddleBaoStatus').val(),
        $('#dtTransactionFrom').val(),
        $('#dtTransactionTo').val(),
        $('#txtPolicyNoFrom').val(),
        $('#txtPolicyNoTo').val(),
        $('#txtProposalNoFrom').val(),
        $('#txtProposalNoTo').val(),
        $('#txteBaoDocFrom').val(),

    ];

    arrayInput.every(function(element, index) {
        if (element != "") {
            check = true;
            return false
        } else return true
    })
    return check;
}