﻿var dataTableMemoMappingModal;
var tableMemoDocument;
var uId;
var frmdata = {
    memoTransactionID: "",
    userID: $("#userName").text(),
    memoDocumentData: [],
    noteList: []
};

function removefile(filename, rowindex) {
    if ($('#chx_' + rowindex).is(":checked")) {
        let linkfile = $('#listFile' + rowindex + ' a:contains("' + filename + '")');
        linkfile.next().next().remove();
        linkfile.next().remove();
        linkfile.remove();
    }
}
$(document).ready(function() {
    uId = $('#memoTransactionID').val();
    frmdata.memoTransactionID = uId;
    $('#btnPreview').on('click', function() {
        window.open(apiUrl + '/FileAttachment/PreviewMemoAttachment/' + $("#ebaoDocumentNo").text(), '_blank');
    });
    loadDDL();
    checkRole();

    function checkRole() {
        if (localStorage.getItem('isAdministrator') == 'true') {
            $('#btnSave_EditItem').css({ pointerEvents: "none" });
            $('#btnEditMemo').css({ pointerEvents: "none" });
        }
    }

    $.ajax({
        type: "GET",
        url: apiUrl + '/Transaction/GetEmemoPrerationDetail?transactionID=' + uId,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function(result) {
            noteList(result.data.noteList)
            memoDocument(result.data.memoAmendmentInformation.documents);
            memoHistory(result.data.memoAmendmentInformation.history);
            loadBasicInformation(result.data.basicInformation);
            loadMemoDocumentInformation(result.data.memoDocumentInformation);
            $('#btnDeleteItem').on('click', function() {
                if ($("input:checkbox[name=checkboxSelect]:checked").length === 0) {
                    alert('กรุณาเลือกรายการที่ต้องการดำเนินการ');
                } else {
                    $("input:checkbox[name=checkboxSelect]:checked").each(function() {
                        var dataSelected = tableMemoDocument.row(this.closest('tr')).data();
                        var memoDocItem = {
                            "memoType": null,
                            "memoDetailID": dataSelected["memoDetailID"],
                            "memoCode": dataSelected["memoCode"],
                            "comment": null,
                            "add": false,
                            "edit": false,
                            "delete": true,
                            "attachments": []
                        };
                        frmdata.memoDocumentData.push(memoDocItem);

                        $(this).closest('tr').hide();
                        // $(this).prop("checked", false);
                    });
                }
            });
        }
    });

    searchModalMemoData();

    $('#ddlMemoType').on('change', function() {
        filterMemoData();
    });
    $('#ddlMemoCode').on('change', function() {
        filterMemoData();
    });

    $('#exampleModalCenter').on('hidden.bs.modal', function(e) {
        resetMemoModal();
    });

    $("#btnModalReset").click(function() {
        resetMemoModal();
    });
    $('#btnModalSearch').click(function() {
        searchModalMemoData();
    });
    $('#btnModalAdd').click(function() {
        var newlists = [];
        $("input:checkbox[name=checkboxModalSelect]:checked").each(function() {
            var dataSelected = dataTableMemoMappingModal.row(this.closest('tr')).data();
            var filteredData = tableMemoDocument
                .column(2)
                .data()
                .filter(function(value, index) {
                    return value == dataSelected["memoCode"] ? true : false;
                });
            if (filteredData.length === 0) {
                // var memoDocItem = {
                //     "memoType": dataSelected["memoTypeName"],
                //     "memoCode": dataSelected["memoCode"],
                //     "comment": null,
                //     "add": true,
                //     "edit": false,
                //     "delete": false,
                //     "attachments": []
                // };
                // frmdata.memoDocumentData.push(memoDocItem);

                var newitem = {
                    "data": {
                        "sequence": null,
                        "memoCode": dataSelected["memoCode"],
                        "memoDescription": dataSelected["description"],
                        "comment": null
                    },
                    "memoType": dataSelected["memoTypeName"]
                }
                newlists.push(newitem);
            }
        });

        $.each(newlists, function(index, item) {
            tableMemoDocument.row.add(item.data).draw(false);
            var i = tableMemoDocument.rows().count() - 1;
            $('#chx_' + i).prop("disabled", false);
            $('#chx_' + i).attr("itemadd", true);
            $('#chx_' + i).attr('memotype', item.memoType);
        });
    });
    resetDisplay();
});
window.onbeforeunload = function(e) {
    return 'Dialog text here.';
};

function searchModalMemoData() {
    $.ajax({
        type: "GET",
        url: apiUrl + '/MasterData/GetMemoMapping',
        contentType: "application/json; charset=utf-8",
        success: function(result) {
            memoList(result.data);
        }
    });
}

function reloadMemoDocument() {
    $.ajax({
        type: "GET",
        url: apiUrl + '/Transaction/GetEmemoPrerationDetail?transactionID=' + uId,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function(result) {
            memoDocument(result.data.memoAmendmentInformation.documents);
        }
    });
}

function resetMemoModal() {
    $('#ddlMemoType').val("");
    $('#ddlMemoCode').val("").trigger("change");
    searchModalMemoData();
}

function saveMemoItems() {
    console.log('Save');
    var btnHtmlBefore = $('#btnSave_EditItem').html();
    buttonLoading('btnSave_EditItem', true);
    if ($("input:checkbox[name=checkboxSelect]:checked").length === 0 &&
        frmdata.memoDocumentData.length === 0 &&
        getNoteItems().length === 0 &&
        $("input:checkbox[name=checkboxSelect][itemadd]").length === 0) {
        alert('กรุณาเลือกรายการที่ต้องการดำเนินการ');
        buttonLoading('btnSave_EditItem', false, btnHtmlBefore);
    } else if ($("input:checkbox[name=checkboxSelect]:checked").length !== 0 &&
        frmdata.memoDocumentData.length !== 0 &&
        $("input:checkbox[name=checkboxSelect][itemadd]").length === 0) {
        //case delete only
        console.log('case delete only')
        callSaveMemoDoc(frmdata, btnHtmlBefore);
    } else if ($("input:checkbox[name=checkboxSelect]:checked").length === 0 &&
        frmdata.memoDocumentData.length === 0 &&
        getNoteItems().length !== 0 &&
        $("input:checkbox[name=checkboxSelect][itemadd]").length === 0) {
        //case note list only
        console.log('case note only')
        callSaveMemoDoc(frmdata, btnHtmlBefore);
    } else {
        console.log('case all');
        $("input:checkbox[name=checkboxSelect]:checked").each(function() {
            $(this).removeAttr('itemadd');
            // console.log(this);
        });
        var rowTotal = $("input:checkbox[name=checkboxSelect]:checked").length;
        rowTotal += $("input:checkbox[name=checkboxSelect][itemadd]").length;
        var rowCount = 1;
        tableMemoDocument.rows().every(function(rowIdx, tableLoop, rowLoop) {
            var data = this.data();
            var chx_attr = $('#chx_' + rowIdx).attr('memotype');
            var chx_attr_exist = (typeof chx_attr !== 'undefined' && chx_attr !== false);
            if ($('#chx_' + rowIdx).is(":checked") || $('#chx_' + rowIdx).is("[itemadd]")) {
                var memoDocItem = {
                    "memoType": (chx_attr_exist) ? chx_attr : null,
                    "memoDetailID": data.memoDetailID,
                    "memoCode": data.memoCode,
                    "comment": $('#comment_' + rowIdx).val(),
                    "add": (chx_attr_exist) ? true : false,
                    "edit": (chx_attr_exist) ? false : true,
                    "delete": false
                };
                let elementfiles = $('#listFile' + rowIdx + ' a');
                let listFiles = new DataTransfer();
                $.each(elementfiles, function(i, v) {
                    (async() => {
                        createFile($(v).text(), v.href).then(function(resultFile) {
                            listFiles.items.add(resultFile);
                        });
                    })()
                });

                setTimeout(function() {
                    let file = $('#file_' + rowIdx)[0].files;
                    if (file.length != 0) {
                        listFiles.items.add(file[0]);
                    }
                    if (listFiles.files.length != 0) {
                        let filesArray = Array.prototype.slice.call(listFiles.files)
                        Promise.all(filesArray.map(getBase64andfile)).then((values) => {
                            let attachment = [];
                            $.each(values, function(i, v) {
                                attachment.push(v);
                            });
                            memoDocItem["attachments"] = attachment;

                            let c = rowCount++;
                            frmdata.memoDocumentData.push(memoDocItem);
                            if (rowTotal === c) {
                                callSaveMemoDoc(frmdata, btnHtmlBefore);
                            }
                        });
                    } else {
                        memoDocItem["attachments"] = null;
                        let c = rowCount++;
                        frmdata.memoDocumentData.push(memoDocItem);
                        if (rowTotal === c) {
                            callSaveMemoDoc(frmdata, btnHtmlBefore);
                        }
                    }
                }, 3000);

                // var file = $('#file_' + rowIdx)[0].files;
                // getBase64(file[0]).then(function(base64string) {
                //     var attachment = [{
                //         "fileName": file[0].name,
                //         "fileBase64": base64string,
                //         "fileSize": file[0].size,
                //         "contentType": file[0].type
                //     }];
                //     memoDocItem["attachments"] = attachment;
                // }).catch(e => {
                //     memoDocItem["attachments"] = null;
                // }).finally(() => {
                //     var c = rowCount++;
                //     // console.log(c);
                //     frmdata.memoDocumentData.push(memoDocItem);
                //     if (rowTotal === c) {
                //         callSaveMemoDoc(frmdata);
                //     }
                // });
            }
        });
    }
}

function callSaveMemoDoc(frmdata, btnHtmlBefore) {
    // console.log(frmdata);
    frmdata.userID = $("#userName").text();
    frmdata.noteList = getNoteItems();

    $.ajax({
        type: "POST",
        url: apiUrl + '/Transaction/SaveMemoDocument',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(frmdata),
        datatype: "json",
        success: function(data) {
            buttonLoading('btnSave_EditItem', false, btnHtmlBefore);
            if (data.success) {
                reloadMemoDocument();
                resetDisplay();
                alert(data.status.message);
            } else if (!data.success) {
                alert(data.status.message);
            } else {
                alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
            }
        },
        error: function() {
            alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
            buttonLoading('btnSave_EditItem', false, btnHtmlBefore);
        }
    });
    frmdata.memoDocumentData = [];
}

loadMemoCodeSelect();
$('#btnEditMemo').click(function() {
    $('#divEditMemo').hide();
    $('.divEditItem').show();
    $("#tableDocument").css("pointer-events", "");
    $("input:checkbox[name=checkboxSelect]").each(function() {
        $(this).prop("disabled", false);
    });
    $('#selectAll').prop('disabled', false)
});

$('#btnSave_EditItem').click(function() {
    saveMemoItems();
});

// $("#exampleModalCenter").on('hide.bs.modal', function() {
//     $('#selectAll_add').prop("checked", false);
//     $("input:checkbox[name=checkboxSelectAdd]").each(function() {
//         $(this).prop("checked", false);
//     });
// });


$('#btnEditItem').click(function() {
    if ($("input:checkbox[name=checkboxSelect]:checked").length === 0) {
        alert('กรุณาเลือกรายการที่ต้องการดำเนินการ');
    } else {
        $("input:checkbox[name=checkboxSelect]:checked").each(function() {
            $('.removefile_' + $(this).val()).show();
            var rowdata = tableMemoDocument.row($(this).val()).data();
            // console.log(rowdata);
            $('#comment_' + $(this).val()).removeAttr("disabled");
            if ($('#comment_' + $(this).val()).val() !== null) {
                $('#comment_' + $(this).val()).val(rowdata.memoDescription);
            }
            $('#file_' + $(this).val()).removeAttr("disabled");
            $('#divFile' + $(this).val()).css("display", "block");
        });
    }
});



$('#selectAll').click(function() {
    var checked = $('#selectAll').prop('checked');
    $("input:checkbox[name=checkboxSelect]").each(function() {
        $(this).prop("checked", checked);
    });
});

$('#selectAll_add').click(function() {
    var checked = $('#selectAll_add').prop('checked');
    $("input:checkbox[name=checkboxSelectAdd]").each(function() {
        $(this).prop("checked", checked);
    });
});

function resetDisplay() {

    $('#divEditMemo').show();
    $('.divEditItem').hide();
    $("#tableDocument").css("pointer-events", "none");
    $('#selectAll').prop('checked', false);
    $('#selectAll').prop('disabled', true);
    $("input:checkbox[name=checkboxSelect]").each(function() {
        $(this).prop("checked", false);
        $(this).prop("disabled", true);
        $('#comment_' + $(this).val()).attr("disabled", true);
        $('#file_' + $(this).val()).attr("disabled", true);
        $('#divFile' + $(this).val()).css("display", "none");
    });
}

function memoDocument(viewModel) {
    if (tableMemoDocument !== null) {
        $('#tableDocument').DataTable().destroy();
        tableMemoDocument = null;
        $('#tableDocument').empty();
    }
    // $("#tableDocument").append('<tfoot><th colspan="6"></th></tfoot>');
    $.fn.dataTable.ext.errMode = 'none';
    tableMemoDocument = $('#tableDocument').DataTable({
        paging: false,
        bInfo: false,
        data: viewModel,
        columns: [{
                targets: 0,
                searchable: false,
                orderable: false,
                className: 'dt-body-center',
                render: function(data, type, full, meta) {
                    return '<input disabled type="checkbox"  name="checkboxSelect" id="chx_' + meta.row + '" value="' + meta.row + '">';
                }
            },
            { data: 'sequence', title: 'SEQ' },
            { data: 'memoCode', title: 'Memo Code' },
            { data: 'memoDescription', title: 'Memo Description' },
            {
                data: 'comment',
                title: 'Comment',
                render: function(data, type, row, meta) {
                    return '<input disabled id="comment_' + meta.row + '" class="form-control text-input" value="' + ((data === null) ? "" : data) + '" />';
                }
            },
            {
                data: 'fileAttachments',
                searchable: false,
                orderable: false,
                render: function(data, type, row, meta) {
                    if (data === undefined) {
                        return '<div><label id="divFile' + meta.row + '" style="display:none" class="file"><input disabled type="file" id="file_' + meta.row + '" aria-label="File browser example" accept=".pdf"><span class="file-custom"></span></label></div>'
                    } else {
                        var html = '<div id="listFile' + meta.row + '">';
                        if (data.length > 0) {
                            $.each(data, function(i, val) {
                                html += '<a href="' + val.filePath + '" target="_blank" style="pointer-events: auto;">' + val.fileName + "</a><span onclick='removefile(\"" + val.fileName + "\",\"" + meta.row + "\")'><i class='fas fa-trash removefile_" + meta.row + " ml-2' style='color:red;cursor:pointer'></i></span><br/>";
                            });
                            $('.removefile_' + meta.row).hide();
                        }
                        html += '<label id="divFile' + meta.row + '" style="display:none" class="file"><input disabled type="file" id="file_' + meta.row + '" aria-label="File browser example" accept=".pdf"><span class="file-custom"></span></label></div>'
                        return html
                    }

                }
            },
        ],
        // footerCallback: function(tfoot, data, start, end, display) {
        //     var remark = viewModel.filter(v => v.memoCode === "");
        //     var html = '<div style="color:red;">'
        //     console.log(remark)
        //     $.each(remark, function(index, value) {
        //         html += '<div name="divRowRemark" class="row mb-2">'
        //         if (index == 0) {
        //             html += '<div class="col-1 text-right">หมายเหตุ&nbsp;&nbsp;&nbsp;:</div>'
        //         } else {
        //             html += '<div class="col-1"></div>'
        //         }
        //         html += '<div id="divRemark_' + index + '" class="col">' + (index + 1) + '.&nbsp;&nbsp;<input id="txtRemarkDes_' + index + '" type="text" size="60" value="' + value.memoDescription + '"'
        //         html += ' style="border: none;border-color: transparent;pointer-events: auto;color:red;"/>&nbsp;&nbsp;'
        //         html += '<button type="button" class="btn btn-sm btn-outline-danger" style="pointer-events: auto;"><i class="fa fa-trash"></i></button>'
        //         html += '</div>'
        //         html += '</div>'
        //     })
        //     html += '</div>'
        //     $(tfoot).find('th').eq(0).html(html);
        // }
    });
}



function memoList(memoData) {
    if (dataTableMemoMappingModal !== null) {
        $('#tableMemoList').DataTable().destroy();
        dataTableMemoMappingModal = null;
        $('#tableMemoList').empty();
    }

    dataTableMemoMappingModal = $('#tableMemoList').DataTable({
        responsive: true,
        autoWidth: false,
        // searching: false,
        paging: false,
        info: false,
        data: memoData,
        columns: [{
                targets: 0,
                searchable: false,
                orderable: false,
                className: 'dt-body-center',
                render: function(data, type, row) {
                    return '<input type="checkbox"  name="checkboxModalSelect" value="' + row["memoCodeID"] + '">';
                }
            },
            { data: "memoTypeID", visible: false },
            { data: 'memoTypeName', title: 'Memo Type' },
            { data: "memoCodeID", visible: false },
            { data: 'memoCode', title: 'Memo Code' },
            { data: 'description', title: 'Detail' }
        ],
    });
    $('#tableMemoList_filter').addClass('d-none');
    filterMemoData();
}

function filterMemoData() {
    var mType = $('#ddlMemoType').val();
    if (mType != "") {
        dataTableMemoMappingModal.column(1).search(mType).draw();
    }
    var mCode = $('#ddlMemoCode').val();
    if (mCode != "") {
        dataTableMemoMappingModal.column(3).search(mCode).draw();
    }
}


function memoHistory(viewModel) {
    $('#tableHistory').dataTable({
        data: viewModel,
        columns: [
            { data: 'sendTo', title: 'Send to' },
            { data: 'channel', title: 'Channel' },
            { data: 'description', title: 'Description' },
            { data: 'sendDate', title: 'Send Date' },
            { data: 'sendTime', title: 'Sent Times' },
            { data: 'sendBy', title: 'Send By' },
            { data: 'sendStatus', title: 'Send Status' },
        ]
    });
}


function loadMemoCodeSelect() {

    var jsonDataCodeList = [{
            "MemoCode": "HX",
            "MemoDescription": "ตรวจสุขภาพโดยแพทย์"
        },
        {
            "MemoCode": "HOLDER",
            "MemoDescription": "ตรวจสุขภาพโดยแพทย์"
        }
    ];
    $('#tableMemoCodeSelect').dataTable({
        data: jsonDataCodeList,
        columns: [{
                targets: 0,
                searchable: false,
                orderable: false,
                className: 'dt-body-center',
                render: function(data, type, full, meta) {
                    return '<input class="form-check-input" type="radio" name="selectcode" value="' + $('<div/>').text(data).html() + '">';
                }
            },
            { data: 'MemoCode', title: 'Memo Code' },
            { data: 'MemoDescription', title: 'Memo Description' }
        ]


    });

}

function loadBasicInformation(viewModel) {
    $('#policyNo').text(viewModel.policyNo);
    $('#proposalNo').text(viewModel.proposalNo);
    $('#proposalDate').text(viewModel.proposalDate);
    $('#policyStatus').text(viewModel.policyStatus);
    $('#policyName').text(viewModel.policyName);
    $('#citizenID').text(viewModel.citizenID);
    $('#mainPlan').text(viewModel.mainPlan);
    $('#agentName').text(viewModel.agentName);
    $('#unit').text(viewModel.unit);
    $('#group').text(viewModel.group);
}

function loadMemoDocumentInformation(viewModel) {
    $('#ebaoDocumentNo').text(viewModel.ebaoDocumentNo);
    $('#ebaoDocumentNo2').text(viewModel.ebaoDocumentNo);
    $('#transactionDate').text(moment(viewModel.transactionDate, 'DD/MM/YYYY').add(543, 'year').format('DD/MM/YYYY'));
    $('#createdUser').text(viewModel.createdUser);
    $('#ebaoMemoStatus').text(viewModel.ebaoMemoStatus);
    $('#ebaoRelpliedDate').text(viewModel.ebaoRelpliedDate);
    $('#ebaoRelpliedUser').text(viewModel.ebaoRelpliedUser);
}

function loadDDL() {
    $("#ddlMemoType").empty();
    $("#ddlMemoType").append("<option value=''>Please select</option>");
    $.ajax({
        url: apiUrl + '/MasterData/GetMemoType',
        success: function(data) {
            $.each(data.data, function(i, val) {
                $("#ddlMemoType").append("<option value=" + val.memoTypeID + ">" + val.memoTypeName + "</option>");
            });
        }
    });


    $('#ddlMemoCode').select2({
        placeholder: 'Please Select',
        cache: true,
        ajax: {
            url: apiUrl + '/MasterData/GetMemoCodeBy',
            dataType: 'json',
            type: 'GET',
            data: function(params) {
                return {
                    param: params.term,
                    type: $("#ddlMemoType").val()
                };
            },
            processResults({ data }) {
                return {
                    results: $.map(data, function(item) {
                        return {
                            text: item.memoCode + ' - ' + item.description,
                            id: item.memoCodeID,
                        }
                    })
                }
            }
        }
    });
}

function noteList(dataSrc) {
    if (dataSrc === undefined) {
        dataSrc = []
    }
    var html = ''
    $.each(dataSrc, function(index, value) {
        var divNoteItem = 'divNoteItem_' + index
        html += '<div id="' + divNoteItem + '" class="row mb-2">'
        html += '<div class="col-10">'
        html += '<input name="notelistitem" type="text" value="' + value + '" class="form-control text-input" />'
        html += '</div>'
        html += '<div class="col"><button class="btn btn-sm" onclick="removeNoteItem(' + divNoteItem + ')">'
        html += '<i class="fa fa-trash" aria-hidden="true"></i></button></div>'
        html += '</div>'
    })
    $('#divNoteList').html(html)

    $('#btnAddNote').on('click', function() {
        var inputTypes = [];
        $('input[name="notelistitem"]').each(function() {
            inputTypes.push($(this).prop('type'));
        });
        var divNoteItem = 'divAddNoteItem_' + inputTypes.length
        var htmlAdd = '<div id="' + divNoteItem + '" class="row mb-2">'
        htmlAdd += '<div class="col-10">'
        htmlAdd += '<input name="notelistitem" type="text" value="" class="form-control text-input" />'
        htmlAdd += '</div>'
        htmlAdd += '<div class="col"><button class="btn btn-sm" onclick="removeNoteItem(' + divNoteItem + ')">'
        htmlAdd += '<i class="fa fa-trash" aria-hidden="true"></i></button></div>'
        htmlAdd += '</div>'
        $('#divNoteList').append(htmlAdd)
    })
}

function removeNoteItem(divNoteItem) {
    divNoteItem.remove();
}

function getNoteItems() {
    var noteItems = []
    $('input[name="notelistitem"]').each(function() {
        noteItems.push($(this).val());
    });
    return noteItems;
}