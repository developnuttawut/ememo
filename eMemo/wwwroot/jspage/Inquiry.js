﻿$(document).ready(function () {
    initDatepicker();
    // $('#transactionDateForm').val(moment().add(543, 'year').format('DD/MM/YYYY'));
    initModalUnit();
    getAgentGroup();
    initModalGroup();
    initEBaoStatusDDL();
    initDeliveryStatusDDL();
    // getAgentCode();
    // getAgentUnit();

    $('#tblDetails').DataTable({
        "scrollX": true,
        "sScrollXInner": "100%",
        columns: [
            { data: 'transactionDate', title: 'Transaction Date' },
            { data: 'documentNo', title: 'eBao Document No.' },
            { data: 'policyNo', title: 'Policy No.' },
            { data: 'proposalNo', title: 'Proposal No.' },
            { data: 'policyName', title: 'Policy Name' },
            { data: 'agentCode', title: 'Agent Code' },
            { data: 'agentName', title: 'Agent Name' },
            { data: 'agentUnit', title: 'Unit' },
            { data: 'agentGroup', title: 'Group' },
            { data: 'ebaoMemoStatus', title: 'eBao Memo Status' },
            { data: 'osMemo', title: 'O/S Memo' },
            { data: 'sending', title: 'Sending' },
        ]
    });
    window.setTimeout(function () {
        reloadData();
    }, 1000);
    
});

function initModalUnit() {
    var tableUnit = $('#tblUnit').DataTable({
        responsive: true,
        autoWidth: false,
        ajax: {
            url: apiUrl + '/MasterData/GetAgentUnit',
            dataSrc: "data",
        },
        columns: [
            { data: null, defaultContent: '<button class="btn btn-primary btn-xs" type="button">Select</button>', orderable: false },
            { data: 'agentUnitCode', title: 'Unit Code' },
            { data: 'agentUnitName', title: 'Unit Name' }
        ],
    });
    $('#tblUnit tbody').on('click', 'button', function () {
        var data = tableUnit.row($(this).parents('tr')).data();
        $('#agentUnit').val(data.agentUnitName);
        $('#modalUnit').modal('hide');
    });
}

function initModalGroup() {
    var tableGroup = $('#tblGroup').DataTable({
        responsive: true,
        autoWidth: false,
        ajax: {
            url: apiUrl + '/MasterData/GetAgentGroup',
            dataSrc: "data",
        },
        columns: [
            { data: null, defaultContent: '<button class="btn btn-primary btn-xs" type="button">Select</button>', orderable: false },
            { data: 'agentGroupCode', title: 'Group Code' },
            { data: 'agentGroupName', title: 'Group Name' }
        ],
    });
    $('#tblGroup tbody').on('click', 'button', function () {
        var data = tableGroup.row($(this).parents('tr')).data();
        $('#txtAgentGroup').val(data.agentGroupName);
        $('#modalGroup').modal('hide');
    });
}

function OnSuccess(response) {
    let invalidEntries = 0
    let arr = response.data.filter(function (item) {
        if (item.ebaoMemoStatus.match(/Printed|Replied|Issued|Discard/i) != null) {
            return true
        }
        invalidEntries++
        return false;
    })
    response.data = arr;
    if (response.data.length === 0) {
        alert('ไม่พบข้อมูล')
        localStorage.removeItem('inquirySearchParams')
    } else {
        $("#msgResultCount").text("Search Results of Memo Inquiry Screen : " + response.data.length + " Items found")

        $('#tblDetails').DataTable().destroy();
        $('#tblDetails').dataTable({
            "scrollX": true,
            "sScrollXInner": "100%",
            data: response.data,
            columns: [{
                data: 'transactionDate',
                title: 'Transaction Date',
                className: 'text-center',
                render: function (data, type, row) {
                    return moment(data, 'DD/MM/YYYY').add(543, 'year').format('DD/MM/YYYY');
                }
            },
            {
                data: 'documentNo',
                title: 'eBao Document No.',
                className: 'text-center',
                render: function (data, type, row) {
                    return '<a href="/eMemo/Inquiry/Information?id=' + row['transactionID'] + '">' + data + '</a>';
                },
            },
            { data: 'policyNo', title: 'Policy No.', className: 'text-center' },
            { data: 'proposalNo', title: 'Proposal No.', className: 'text-center' },
            { data: 'policyName', title: 'Policy Name' },
            { data: 'agentCode', title: 'Agent Code', className: 'text-center' },
            { data: 'agentName', title: 'Agent Name' },
            { data: 'agentUnit', title: 'Unit' },
            { data: 'agentGroup', title: 'Group' },
            { data: 'ebaoMemoStatus', title: 'eBao Memo Status', className: 'text-center' },
            { data: 'osMemo', title: 'O/S Memo', className: 'text-center' },
            { data: 'sending', title: 'Sending', className: 'text-center' },
            ],
            bDestroy: true
        });
    }

};


function SearchValue() {
    var InquiryModel = {
        "documentNo": $("#documentNo").val(),
        "transactionDateForm": convertThaiToUS($("#transactionDateForm").val()),
        "transactionDateTo": convertThaiToUS($("#transactionDateTo").val()),
        "policyNo": $("#policyNo").val(),
        "proposalNo": $("#proposalNo").val(),
        "ebaoMemoStatusID": $("#ebaoMemoStatusID").val(),
        "osMemo": $("#osMemo").val(),
        "sending": $("#sending").val(),
        "citizenID": $("#citizenID").val(),
        "partyID": $("#partyID").val(),
        "customerName": $("#customerName").val(),
        "customerSurname": $("#customerSurname").val(),
        "agentCode": $("#agentCode").val(),
        "agentName": $("#agentName").val(),
        "agentSurname": $("#agentSurname").val(),
        "agentUnit": $("#agentUnit").val(),
        "agentGroup": $("#agentGroup").val(),
    }
    localStorage.setItem("inquirySearchParams", JSON.stringify(InquiryModel));
    $.ajax({
        type: "POST",
        url: apiUrl + '/Inquiry/GetMemoInquiry',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(InquiryModel),
        datatype: "json",
        success: OnSuccess
    });
}
function reloadData() {
    var searchParams = localStorage.getItem('inquirySearchParams')
    if (!!searchParams) {
        var val = JSON.parse(searchParams)
        $("#documentNo").val(val.documentNo);
        $("#transactionDateForm").val((!!val.transactionDateForm) ? moment(val.transactionDateForm).add(543, 'year').format('DD/MM/YYYY') : "");
        $("#transactionDateTo").val((!!val.transactionDateTo) ? moment(val.transactionDateTo).add(543, 'year').format('DD/MM/YYYY') : "");
        $("#policyNo").val(val.policyNo);
        $("#proposalNo").val(val.proposalNo);
        $("#ebaoMemoStatusID").val(val.ebaoMemoStatusID).trigger('change');
        $("#osMemo").val(val.osMemo);
        $("#sending").val(val.sending).trigger('change');
        $("#citizenID").val(val.citizenID);
        $("#partyID").val(val.partyID);
        $("#customerName").val(val.customerName);
        $("#customerSurname").val(val.customerSurname);
        $("#agentCode").val(val.agentCode);
        $("#agentName").val(val.agentName);
        $("#agentSurname").val(val.agentSurname);
        $("#agentUnit").val(val.agentUnit);
        $("#agentGroup").val(val.agentGroup);
        $.ajax({
            type: "POST",
            url: apiUrl + '/Inquiry/GetMemoInquiry',
            contentType: "application/json; charset=utf-8",
            data: searchParams,
            datatype: "json",
            success: OnSuccess
        });
    }
}

function ClearValue() {
    $("#documentNo").val("");
    //$("#transactionDateForm").val(moment().add(543, 'year').format('DD/MM/YYYY'));
    $("#transactionDateForm").val("");
    $("#transactionDateTo").val("");
    $("#policyNo").val("");
    $("#proposalNo").val("");
    $("#ebaoMemoStatusID").val("").trigger('change');
    $("#osMemo").val("");
    $("#sending").val("").trigger('change');
    $("#citizenID").val("");
    $("#partyID").val("");
    $("#customerName").val("");
    $("#customerSurname").val("");
    $("#agentCode").val("");
    $("#agentName").val("");
    $("#agentSurname").val("");
    $("#agentUnit").val("");
    $("#agentGroup").val("");
    $('#tblDetails').dataTable().fnClearTable();
    $("#msgResultCount").text("Search Results of Memo Inquiry Screen : " + 0 + " Items found")
    localStorage.removeItem('inquirySearchParams')
}

function getAgentCode() {
    $("#agentCode").empty();
    $("#agentCode").append("<option value=''>Please select</option>");
    $.ajax({
        //url: 'https://localhost:44312/api/Master/2',
        url: apiUrl + '/MasterData/GetBranchOffice',
        success: function (data) {
            $.each(data, function (i, val) {
                $("#agentCode").append("<option value='" + val.code + "'>" + val.message + "</option>");
            });
        }
    });

}

function getAgentUnit() {
    $(".agentUnit").empty();
    $(".agentUnit").append("<option value=''>Please select</option>");
    $.ajax({
        //url: 'https://localhost:44312/api/Master/1',
        url: apiUrl + '/MasterData/GetAgentUnit',
        success: function (data) {
            $.each(data, function (i, val) {
                $(".agentUnit").append("<option value='" + val.code + "'>" + val.message + "</option>");
            });
        }
    });
}

function getAgentGroup() {


    $(".agentGroup").empty();
    $(".agentGroup").append("<option value=''>Please select</option>");
    $.ajax({
        //url: 'https://localhost:44312/api/Master/1',
        url: apiUrl + '/MasterData/GetAgentGroup',
        success: function (data) {
            $.each(data, function (i, val) {
                $(".agentGroup").append("<option value='" + val.code + "'>" + val.message + "</option>");
            });
        }
    });

}

function initEBaoStatusDDL() {
    $.ajax({
        url: apiUrl + '/Transaction/GetEBaoStatus',
        dataType: 'json',
        type: 'GET'
    }).done(function (data) {
        let invalidEntries = 0
        let arr = data.data.filter(function (item) {
            if (item.description.match(/Printed|Replied|Issued|Discard/i) != null) {
                return true
            }
            invalidEntries++
            return false;
        })
        var dataSource = $.map(arr, function (item) {
            return {
                text: item.description,
                id: item.eBaoStatusID,
            }
        });
        $('#ebaoMemoStatusID').select2({
            // placeholder: 'All',
            data: dataSource,
            minimumResultsForSearch: -1
        });
    });
}

function initDeliveryStatusDDL() {
    $.ajax({
        url: apiUrl + '/Transaction/GetDeliveryStatus',
        dataType: 'json',
        type: 'GET'
    }).done(function (data) {
        let invalidEntries = 0
        let arr = data.data.filter(function (item) {
            if (item.description.match(/Successfully|Unsuccessfully/i) != null) {
                return true
            }
            invalidEntries++
            return false;
        })
        var dataSource = $.map(arr, function (item) {
            return {
                text: item.description,
                id: item.deliveryStatusID,
            }
        });
        $('#sending').select2({
            // placeholder: 'All',
            data: dataSource,
            minimumResultsForSearch: -1
        });
    });
}
