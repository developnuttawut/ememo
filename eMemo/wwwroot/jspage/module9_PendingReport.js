$(document).ready(function() {
    initDatepicker();
    init_ddl_productCategory();
    $('#btnSearchPendingPreview').click(function() {
        var asOfDate = convertThaiToUS($('#asOfDate').val());
        if (isValidDate(asOfDate)) {
            prepareData(asOfDate);
        } else {
            alert('กรุณาระบุวันที่ต้องการค้นหา')
        }
    });

    $('#btnClear').on('click', function() {
        clearInput();
    });
});

function prepareData(asOfDate) {
    var frmdata = {
        asOfDate: asOfDate.toJSON(),
        productCategory: $('#ddlProductCategory').val()
    };
    searchReport('btnSearchPendingPreview', frmdata, 'PreviewPendingReport', 'ExportPendingReport');
}