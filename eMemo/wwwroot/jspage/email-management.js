﻿$(document).ready(function () {
    loadEmailManagement();
    initBusinessUnitDdl();
    initBranchOfficeDdl();
    initBranchOfficeDdlMod();
    var dataTableGroupEmail;
    checkRole();

    function checkRole() {
        if (localStorage.getItem('isAdministrator') == 'true') {
            $('#btnAddEmailGroup').css({ pointerEvents: "none" });
        }
    }

    function loadEmailManagement() {


        if (dataTableGroupEmail !== null) {
            $('#tableGroupEmail').DataTable().destroy();
            dataTableGroupEmail = null;
            $('#tableGroupEmail').empty();
        }
        dataTableGroupEmail = $('#tableGroupEmail').DataTable({
            language: {
                "infoFiltered": ""
            },
            "scrollX": true,
            "sScrollXInner": "100%",
            ajax: {
                type: "GET",
                url: apiUrl + '/MasterData/GetGroupEmailManagement',
                contentType: "application/json; charset=utf-8",
                dataSrc: function (result) {
                    return result.data;
                }
            },
            columns: [{
                title: 'Use',
                targets: 0,
                searchable: false,
                orderable: false,
                className: 'text-center',
                render: function (data, type, full, meta) {
                    return '<input type="radio" name="selectemail" class="selectemail" data-wasChecked="false" />';
                }
            },
            { data: "groupEmailManagementID", visible: false },
            { data: 'emailType', title: 'Type' },
            { data: "businessUnitID", visible: false },
            { data: 'businessUnit', title: 'Business Unit' },
            { data: "branchOfficeID", visible: false },
            { data: 'branchOffice', title: 'Branch / Office' },
            { data: "toEmail", visible: false },
            {
                data: 'toEmail',
                title: 'To e-mail',
                render: function (data, type, full, meta) {
                    var email = "";
                    for (var i = 0; i < data.length; i++) {
                        email += data[i] + "<br>"
                    }
                    return data.length == 0 ? "" : email;
                }
            },
            { data: "ccEmail", visible: false },
            {
                data: 'ccEmail',
                title: 'CC e-mail',
                render: function (data, type, full, meta) {
                    var email = "";
                    for (var i = 0; i < data.length; i++) {
                        email += data[i] + "<br>"
                    }
                    return data.length == 0 ? "" : email;
                }
            },
            {
                data: 'status',
                visible: false,
                render: function (data, type, full, meta) {
                    var email = "";
                    for (var i = 0; i < full.ccEmail.length; i++) {
                        email += full.ccEmail[i] + " "
                    }
                    for (var i = 0; i < full.toEmail.length; i++) {
                        email += full.toEmail[i] + " "
                    }
                    return data.length == 0 ? "" : email;
                }
            },
            { data: "status", visible: false },
            {
                data: "sendGroup",
                title: 'Send Group'
            },
            {
                data: 'status',
                title: 'Status',
                className: 'text-center',
                render: function (data, type, full, meta) {
                    return data ? "Enable" : "Disable";
                }
            }


            ]

        });
        dataTableGroupEmail.column(2).search($('#ddlEmailTypeMain option:selected').text()).draw();
    }

    $("#btnAddEmail").click(function () {
        var txtEmailAddress = $("#txtEmailAddress").val().trim();
        var radioTOCC = $('input[name="radioTOCC"]:checked').val();
        if (txtEmailAddress == "") {
            alert("กรุณาระบุ Email Address");
        } else {
            if (isValidEmail(txtEmailAddress)) {
                var tdTOCC = '<td class="to_cc" style="text-align:center">' + radioTOCC + '</td>'
                var tdEmailAddress = '<td class="email" style="text-align:center">' + txtEmailAddress + '</td>'
                var tdBtnDelete = '<td style="text-align:center"><ion-icon style="color:green;font-size:20px;cursor:pointer" name="trash-bin-outline" class="deleteEmail"></ion-icon></td>';
                var newRow = '<tr>' + tdTOCC + tdEmailAddress + tdBtnDelete + '</tr>';
                $('#tableEmailManagement').append(newRow);
                $("#txtEmailAddress").val("");
            } else {
                alert("Email Address ไม่ถูกต้อง");
            }

        }

    });

    function isValidEmail(emailText) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailText);
    };


    $("#btnSave").click(function () {
        var ddlEmailTypeModify = $("#ddlEmailTypeModify").val();
        var ddlBusinessUnitID = $("#ddlBusinessUnitModify").val();
        var ddlSendGroup = $("#ddlSendGroup").val();
        var ddlBusinessUnit = $('#ddlBusinessUnitModify option:selected').text();
        var ddlBranchOfficeID = $("#ddlBranchOfficeModify").val();
        var ddlBranchOffice = $("#ddlBranchOfficeModify option:selected").text();
        var txtEmailGroupID = $("#txtEmailGroupID").val();
        var status = $('input[name="radioStatus"]:checked').val() == "true";
        var groupEmailList = [];

        $('#tableEmailManagement tr').each(function () {
            var to_cc = $(this).find('td.to_cc').text();
            var email = $(this).find('td.email').text();

            if (to_cc != "") {
                groupEmailList.push({ to_cc: to_cc, emailAddress: email });
            }
        });

        var frmdata = {
            ID: txtEmailGroupID == "" ? null : parseInt(txtEmailGroupID),
            emailType: ddlEmailTypeModify,
            businessUnitID: ddlBusinessUnitID == "" ? null : parseInt(ddlBusinessUnitID),
            businessUnit: ddlBusinessUnit,
            branchOfficeID: ddlBranchOfficeID == "" ? null : ddlBranchOfficeID,
            sendGroup: ddlSendGroup,
            branchOffice: ddlBranchOffice,
            status: status,
            groupEmailList: groupEmailList,
            userID: $("#userName").text()
        };
        $.ajax({
            type: "POST",
            url: apiUrl + '/MasterData/SaveGroupEmailManagement',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(frmdata),
            datatype: "json",
            success: function (data) {
                if (data.success) {
                    $("#modalAddEmail").modal("hide");
                    $('#txtEmailGroupID').val("");
                    alert(data.status.message)

                    dataTableGroupEmail.ajax.reload();

                } else if (!data.success) {
                    alert(data.status.message);
                } else {
                    alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
                }
            }
        });

    });

    $("#tableEmailManagement").on("click", ".deleteEmail", function () {
        $(this).closest("tr").remove();
    });
    $("#btnAddEmailGroup").click(function () {
        if ($('#txtEmailGroupID').val() == "") {
            clearEmailManagement();
        }

        $("#modalAddEmail").modal("show");
    });

    function clearEmailManagement() {
        $('#ddlEmailTypeModify').val("Sale Support").trigger("change");
        $('#ddlBranchOfficeModify').val("").trigger("change");
        $('#ddlBusinessUnitModify').val("").trigger("change");
        $('#txtEmailGroupID').val("");
        $('#radioStatus').prop("checked", true);
        $('#tableEmailManagement  tr').slice(1).remove();
        checkRequire();
    }


    $(document).on('click', '.selectemail', function () {
        var wasChecked = $(this).attr('data-wasChecked');

        if (wasChecked == "true") {
            this.checked = !this.checked;
            $(this).attr('data-wasChecked', false);
            clearEmailManagement();
        } else {
            $(this).attr('data-wasChecked', true);
        }

        if (this.checked == true) {
            var data = dataTableGroupEmail.row(this.closest('tr')).data();
            clearEmailManagement();
            $('#txtEmailGroupID').val(data.groupEmailManagementID);
            $('#ddlEmailTypeModify').val(data.emailType).trigger("change");
            setTimeout(function () {
                $('#ddlSendGroup').val(data.sendGroup).trigger("change");
            }, 1000);


            checkRequire();

            $('#ddlBusinessUnitModify').val(data.businessUnitID).trigger("change");

            bindBranchOfficeDdlMod(data.branchOfficeID);

            if (data.status) {
                $('#radioStatus').prop("checked", true)
            } else {
                $('#radioStatusDisable').prop("checked", true)
            }

            for (var i = 0; i < data.toEmail.length; i++) {
                var tdTOCC = '<td class="to_cc" style="text-align:center">TO</td>'
                var tdEmailAddress = '<td class="email" style="text-align:center">' + data.toEmail[i] + '</td>'
                var tdBtnDelete = '<td style="text-align:center"><ion-icon style="color:green;font-size:20px;cursor:pointer" name="trash-bin-outline" class="deleteEmail"></ion-icon></td>';
                var newRow = '<tr>' + tdTOCC + tdEmailAddress + tdBtnDelete + '</tr>';
                $('#tableEmailManagement').append(newRow);
            }
            for (var i = 0; i < data.ccEmail.length; i++) {
                var tdTOCC = '<td class="to_cc" style="text-align:center">CC</td>'
                var tdEmailAddress = '<td class="email" style="text-align:center">' + data.ccEmail[i] + '</td>'
                var tdBtnDelete = '<td style="text-align:center"><ion-icon style="color:green;font-size:20px;cursor:pointer" name="trash-bin-outline" class="deleteEmail"></ion-icon></td>';
                var newRow = '<tr>' + tdTOCC + tdEmailAddress + tdBtnDelete + '</tr>';
                $('#tableEmailManagement').append(newRow);
            }

        }
    });


    $("#btnResetModify").click(function () {
        clearEmailManagement();
        $("#tableGroupEmail").find("td input[data-wasChecked='true']").prop("checked", false);
        $("#tableGroupEmail").find("td input[data-wasChecked='true']").attr('data-wasChecked', false)

    });

    $("#ddlEmailTypeModify").change(function () {
        checkRequire();
        bindSendGroup();
    });

    function bindSendGroup() {

        if ($("#ddlEmailTypeModify").val() == "Sale Support") {
            $.ajax({
                url: apiUrl + '/MasterData/GetSaleGroup',
                dataType: 'json',
                type: 'GET',
                success: function (result) {
                    populateSelect(result.data, 'string', 'string');
                }
            });

        } else {
            $.ajax({
                url: apiUrl + '/MasterData/GetUnderwritingGroup',
                dataType: 'json',
                type: 'GET',
                success: function (result) {
                    populateSelect(result.data, 'string', 'string');
                }
            });
        }
    }

    function populateSelect(data, idType, nameType) {
        if (!data || !data[0]) {
            return;
        }
        $('#ddlSendGroup option').remove();
        var select = document.getElementById('ddlSendGroup');
        var keys = Object.keys(data[0]);
        var idKey = typeof (parseInt(keys[0])) == idType ? keys[0] : keys[1];
        var nameKey = typeof (parseInt(keys[0])) == nameType ? keys[0] : keys[1];

        for (var i = 0; i < data.length; i++) {
            var option = document.createElement('option');
            option.value = data[i][idKey];
            option.label = data[i][nameKey];
            select.appendChild(option);
        }

        $("#ddlSendGroup").prepend("<option value='' selected='selected'>Please Select</option>");
    }

    function checkRequire() {
        if ($("#ddlEmailTypeModify").val() == "Sale Support") {
            $(".requireSale").show();
        } else {
            $(".requireSale").hide();
        }
    }

    $("#btnResetMain").click(function () {
        $('#ddlEmailTypeMain').val("Sale Support");

        $('#ddlBusinessUnitMain').val("").trigger("change");
        $('#ddlStatusMain').val("");
        $('#txtEmailMain').val("");

        loadEmailManagement();
        clearEmailManagement();

        $("#tableGroupEmail").find("td input[data-wasChecked='true']").prop("checked", false);
        $("#tableGroupEmail").find("td input[data-wasChecked='true']").attr('data-wasChecked', false)
    });

    $('#ddlEmailTypeMain').on('change', function (e) {
        var status = $('#ddlEmailTypeMain option:selected').text();
        dataTableGroupEmail.column(2).search(status).draw();
    });
    $('#ddlBusinessUnitMain').on('change', function (e) {
        initBranchOfficeDdl();
        var status = $('#ddlBusinessUnitMain option:selected').text();
        dataTableGroupEmail.column(4).search(status).draw();
    });

    $('#ddlBranchOfficeMain').on('change', function (e) {
        var status = $('#ddlBranchOfficeMain option:selected').text();
        dataTableGroupEmail.column(6).search(status).draw();
    });

    $('#ddlStatusMain').on('change', function (e) {
        var status = $(this).val();

        if (status == "true") {
            dataTableGroupEmail.column(12).search(true).draw();
        } else {
            dataTableGroupEmail.column(12).search(false).draw();
        }

    });
    $('#txtEmailMain').keyup(function () {
        var status = $(this).val();
        dataTableGroupEmail.columns(11).search(status).draw();
    });

    $('#ddlBusinessUnitModify').on('change', function (e) {
        initBranchOfficeDdlMod();
    });
});

function initBusinessUnitDdl() {
    $.ajax({
        url: apiUrl + '/MasterData/GetBusinessUnit',
        dataType: 'json',
        type: 'GET'
    }).done(function (data) {
        var channelData = $.map(data.data, function (item) {
            return {
                text: item.businessUnitID + " : " + item.description,
                id: item.businessUnitID,
            }
        });
        $('#ddlBusinessUnitMain').select2({
            placeholder: 'Please Select',
            data: channelData,
            sorter: function (data) {
                return data.sort(function (a, b) {
                    var n1 = parseInt(a.id);
                    var n2 = parseInt(b.id);
                    return n1 < n2 ? -1 : n1 > n2 ? 1 : 0;
                });
            }
        });
        $('#ddlBusinessUnitModify').select2({
            placeholder: 'Please Select',
            data: channelData,
            sorter: function (data) {
                return data.sort(function (a, b) {
                    var n1 = parseInt(a.id);
                    var n2 = parseInt(b.id);
                    return n1 < n2 ? -1 : n1 > n2 ? 1 : 0;
                });
            }
        });
    });
}

function initBranchOfficeDdl() {
    var unitId = $('#ddlBusinessUnitMain').val();
    $('#ddlBranchOfficeMain').empty().trigger("change");
    if (unitId !== '') {
        $.ajax({
            url: apiUrl + '/MasterData/GetBranchOfficeByUnit/' + unitId,
            dataType: 'json',
            type: 'GET'
        }).done(function (data) {
            var channelData = $.map(data.data, function (item) {
                return {
                    text: item.branchOfficeID + " : " + item.description,
                    id: item.branchOfficeID,
                }
            });
            $('#ddlBranchOfficeMain').select2({
                placeholder: 'Please Select',
                data: channelData,
                sorter: function (data) {
                    return data.sort(function (a, b) {
                        var n1 = parseInt(a.id);
                        var n2 = parseInt(b.id);
                        return n1 < n2 ? -1 : n1 > n2 ? 1 : 0;
                    });
                }
            });
            $('#ddlBranchOfficeMain').val("").trigger("change")
        });
    } else {
        $('#ddlBranchOfficeMain').select2({
            placeholder: 'Please Select',
        });
    }
}

function initBranchOfficeDdlMod() {
    var unitId = $('#ddlBusinessUnitModify').val();
    $('#ddlBranchOfficeModify').empty().trigger("change");
    if (unitId !== '') {
        $.ajax({
            url: apiUrl + '/MasterData/GetBranchOfficeByUnit/' + unitId,
            dataType: 'json',
            type: 'GET'
        }).done(function (data) {
            var channelData = $.map(data.data, function (item) {
                return {
                    text: item.branchOfficeID + " : " + item.description,
                    id: item.branchOfficeID,
                }
            });
            $('#ddlBranchOfficeModify').select2({
                placeholder: 'Please Select',
                data: channelData,
                sorter: function (data) {
                    return data.sort(function (a, b) {
                        var n1 = parseInt(a.id);
                        var n2 = parseInt(b.id);
                        return n1 < n2 ? -1 : n1 > n2 ? 1 : 0;
                    });
                }
            });
            if ($('#txtEmailGroupID').val() == "") {
                $('#ddlBranchOfficeModify').val("").trigger("change")
            }
        });
    } else {
        $('#ddlBranchOfficeModify').select2({
            placeholder: 'Please Select',
        });
    }
}

function bindBranchOfficeDdlMod(selectId) {
    var unitId = $('#ddlBusinessUnitModify').val();
    $('#ddlBranchOfficeModify').empty().trigger("change");
    $.ajax({
        url: apiUrl + '/MasterData/GetBranchOfficeByUnit/' + unitId,
        dataType: 'json',
        type: 'GET'
    }).done(function (data) {
        var channelData = $.map(data.data, function (item) {
            return {
                text: item.branchOfficeID + " : " + item.description,
                id: item.branchOfficeID,
            }
        });
        $('#ddlBranchOfficeModify').select2({
            placeholder: 'Please Select',
            data: channelData,
            sorter: function (data) {
                return data.sort(function (a, b) {
                    var n1 = parseInt(a.id);
                    var n2 = parseInt(b.id);
                    return n1 < n2 ? -1 : n1 > n2 ? 1 : 0;
                });
            }
        });
        $('#ddlBranchOfficeModify').val(selectId).trigger("change")
    });
}