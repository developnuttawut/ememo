$(document).ready(function() {
    initDatepicker();
    initDeliveryStatus();
    $('#btnSearchSendingMemoPreview').click(function() {
        var beginDate = convertThaiToUS($('#beginDateMemoPrinted').val());
        var endDate = convertThaiToUS($('#endDateMemoPrinted').val());
        var deliveryStatus = $('#ddlDeliveryStatus').find(":selected").val();
        var resendNeed = $('#ddlResendNeed').find(":selected").val()
        var showLatest = $('#ddlDisplayMode').find(":selected").val()
        if (isValidDate(beginDate)) {
            if (isValidDate(endDate)) {
                //กรณีระบุวันที่สิ้นสุด ตรวจสอบวันที่เริ่มต้นมากกว่าวันที่สิ้นสุด
                if (beginDate.getTime() > endDate.getTime()) {
                    alert('กรุณาระบุวันที่ที่ต้องการค้นหาให้ถูกต้อง')
                } else {
                    //Search
                    prepareData(beginDate, endDate, deliveryStatus, resendNeed, showLatest);
                }
            } else {
                //ถ้าระบุเงื่อนไขในส่วนวันที่เริ่มต้นแต่ไม่ระบุวันที่สิ้นสุด ระบบจะแสดงข้อมูลสำหรับวันที่ผู้ใช้งานระบุเท่านั้น
                //search
                prepareData(beginDate, endDate, deliveryStatus, resendNeed, showLatest);
            }
        } else {
            alert('กรุณาระบุวันที่ต้องการค้นหาเริ่มต้น')
        }
    });

    $('#btnClear').on('click', function() {
        clearInput();
        initDeliveryStatus();
        $('#ddlDisplayMode').val("false").trigger('change');
        $('#ddlResendNeed').val("");
    });

    $('#ddlDisplayMode').select2({ minimumResultsForSearch: -1 });
    $('#ddlResendNeed').select2({ minimumResultsForSearch: -1 });
});

function prepareData(beginDate, endDate, deliveryStatus, resendNeed, showLatest) {
    // displayData();
    var frmdata = {
        memoPrintedDateFrom: beginDate.toJSON(),
        memoPrintedDateTo: endDate.toJSON(),
        deliveryStatus: deliveryStatus,
        resendNeed: resendNeed,
        showLatest: (showLatest === 'true')
    };
    searchReport('btnSearchSendingMemoPreview', frmdata, 'PreviewSendingMemoReport', 'ExportSendingMemoReport');
}

function initDeliveryStatus() {
    $('#ddlDeliveryStatus').empty().trigger('change');
    $.ajax({
        url: apiUrl + '/Transaction/GetDeliveryStatus',
        dataType: 'json',
        type: 'GET'
    }).done(function(data) {
        let invalidEntries = 0
        let arr = data.data.filter(function(item) {
            if (item.description.match(/Successfully|Unsuccessfully/i) != null) {
                return true
            }
            invalidEntries++
            return false;
        })
        var datasource = $.map(arr, function(item) {
            return {
                text: item.description,
                id: item.deliveryStatusID,
            }
        });
        $('#ddlDeliveryStatus').select2({
            data: datasource,
            minimumResultsForSearch: -1
        });
    });

}