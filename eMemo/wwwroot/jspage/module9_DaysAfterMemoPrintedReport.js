$(document).ready(function() {
    initDatepicker();
    $('#btnSearchDayAfterMemoPrintedPreview').click(function() {
        var beginDate = convertThaiToUS($('#beginDateMemoPrinted').val());
        var endDate = convertThaiToUS($('#endDateMemoPrinted').val());
        var condition = $('#ddlCondition').find(":selected").val()
        var days = $('#txtPrintedDay').val();
        if (isValidDate(beginDate)) {
            if (isValidDate(endDate)) {
                //กรณีระบุวันที่สิ้นสุด ตรวจสอบวันที่เริ่มต้นมากกว่าวันที่สิ้นสุด
                if (beginDate.getTime() > endDate.getTime()) {
                    alert('กรุณาระบุวันที่ที่ต้องการค้นหาให้ถูกต้อง')
                } else {
                    //Search
                    prepareData(beginDate, endDate, condition, days);
                }
            } else {
                //ถ้าระบุเงื่อนไขในส่วนวันที่เริ่มต้นแต่ไม่ระบุวันที่สิ้นสุด ระบบจะแสดงข้อมูลสำหรับวันที่ผู้ใช้งานระบุเท่านั้น
                //search
                prepareData(beginDate, endDate, condition, days);
            }
        } else {
            alert('กรุณาระบุวันที่ต้องการค้นหาเริ่มต้น')
        }
    });

    $('#btnClear').on('click', function() {
        $('#txtPrintedDay').val(0);
        clearInput();
    });
});

function prepareData(beginDate, endDate, condition, days) {
    var frmdata = {
        memoPrintedDateFrom: beginDate.toJSON(),
        memoPrintedDateTo: endDate.toJSON(),
        condition: condition,
        printedDay: parseInt(days)
    };
    searchReport('btnSearchDayAfterMemoPrintedPreview', frmdata, 'Preview15DayAfterPrintedReport', 'Export15DayAfterPrintedReport');
}