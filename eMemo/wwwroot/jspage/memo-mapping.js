﻿var listFiles = new DataTransfer();

function removefile(filename) {
    let temp = new DataTransfer();
    for (var i = 0; i < listFiles.files.length; i++) {
        if (listFiles.files[i].name != filename) {
            temp.items.add(listFiles.files[i]);
        }
    }
    listFiles = temp;
    let linkfile = $('#listFile a:contains("' + filename + '")');
    linkfile.next().next().remove();
    linkfile.next().remove();
    linkfile.remove();
}
$(document).ready(function() {
    var dataTableMemoTypeList;
    var dataTableMemoCodeList;
    var dataTableMemoMapping;



    loadMemoMapping();
    loadMemoTypeList();
    loadMemoCodeList();
    loadDDL();
    checkRole();

    function checkRole() {
        if (localStorage.getItem('isAdministrator') == 'true') {
            $('#showModalAddMemoType').css({ pointerEvents: "none" });
            $('#showModalAddMemoCode').css({ pointerEvents: "none" });
        }
    }

    $('#selectcode').selectize({
        sortField: 'text'
    });

    function loadMemoMapping() {
        if (dataTableMemoMapping !== null) {
            $('#tableMemoMapping').DataTable().destroy();
            dataTableMemoMapping = null;
            $('#tableMemoMapping').empty();
        }

        dataTableMemoMapping = $('#tableMemoMapping').DataTable({
            "scrollX": true,
            "sScrollXInner": "100%",
            "ajax": {
                type: "GET",
                url: apiUrl + '/MasterData/GetMemoMapping',
                contentType: "application/json; charset=utf-8",
                dataSrc: function(result) {
                    return result.data;
                }
            },
            "columns": [

                { data: "memoTypeID", visible: false },
                { data: 'memoTypeName', title: 'Memo Type' },
                {
                    data: 'memoTypeSequence',
                    title: 'Memo Type Sequence',
                    className: 'text-center'
                },
                { data: "memoCodeID", visible: false },
                { data: 'memoCode', title: 'Memo Code' },
                { data: 'description', title: 'Memo Description' },
                {
                    data: 'fileAttachment',
                    title: 'Attached',
                    render: function(data, type, row) {
                        var listFileName = "";
                        for (var i = 0; i < row.fileAttachment.length; i++) {
                            listFileName += "<a href='" + row.fileAttachment[i].filePath + "' target='_blank'><ion-icon style='color:red;font-size:15px;cursor:pointer' name='document-attach'></ion-icon>" + row.fileAttachment[i].fileName + "</a> ";
                            if (i != row.fileAttachment.length - 1) {
                                listFileName += "</br>";
                            }
                        }

                        return listFileName;
                    }
                },
                {
                    data: 'memoCodeSequence',
                    title: 'Memo Sequence',
                    className: 'text-center'
                },
                { data: "status", visible: false },
                {
                    data: 'status',
                    title: 'Status',
                    className: 'text-center',
                    render: function(data, type, row) {
                        return row.status == true ? 'Enable' : 'Disable';
                    }
                }
            ]


        });



    }

    function loadMemoTypeList() {
        if (dataTableMemoTypeList !== null) {
            $('#tableMemoTypeList').DataTable().destroy();
            dataTableMemoTypeList = null;
            $('#tableMemoTypeList').empty();
        }

        dataTableMemoTypeList = $('#tableMemoTypeList').DataTable({
            "ajax": {
                type: "GET",
                url: apiUrl + '/MasterData/GetMemoType',
                contentType: "application/json; charset=utf-8",
                dataSrc: function(result) {
                    return result.data;
                }
            },
            "order": [],
            "columns": [{
                    title: "Use",
                    render: function(data, type, row) {
                        return '<input type="radio" name="selectmemotype" class="selectmemotype" />';
                    },
                    className: "text-center",
                    visible: true,
                    searchable: false,
                    orderable: false
                },
                { data: "memoTypeID", visible: false, searchable: false },
                { data: 'memoTypeName', title: 'Memo Type' },
                { data: 'memoTypeSequence', title: 'Memo Type Sequence', className: 'text-center' },
            ]
        });
    }

    function loadMemoCodeList() {
        if (dataTableMemoCodeList !== null) {
            $('#tableMemoCodeListFirst').DataTable().destroy();
            dataTableMemoCodeList = null;
            $('#tableMemoCodeList').empty();
        }

        dataTableMemoCodeList = $('#tableMemoCodeList').DataTable({
            "ajax": {
                type: "GET",
                url: apiUrl + '/MasterData/GetMemoMapping',
                contentType: "application/json; charset=utf-8",
                dataSrc: function(result) {
                    return result.data;
                }
            },
            "columns": [{
                    title: "Use",
                    render: function(data, type, row) {
                        return '<input type="radio" name="selectmemocode" class="selectmemocode" />';
                    },
                    className: "text-center",
                    visible: true,
                    searchable: false,
                    orderable: false
                },
                { data: "memoTypeID", visible: false, searchable: false },
                { data: 'memoTypeName', title: 'Memo Type' },
                { data: 'memoTypeSequence', title: 'Memo Type Sequence', className: 'text-center' },
                { data: "memoCodeID", visible: false, searchable: false },
                { data: 'memoCode', title: 'Memo Code' },
                { data: 'description', title: 'Memo Description' },
                {
                    data: 'fileAttachment',
                    title: 'Attach',
                    render: function(data, type, row) {
                        var listFileName = "";
                        for (var i = 0; i < row.fileAttachment.length; i++) {
                            listFileName += "<a href='" + row.fileAttachment[i].filePath + "' target='_blank'><ion-icon style='color:red;font-size:15px;cursor:pointer' name='document-attach'></ion-icon>" + row.fileAttachment[i].fileName + "</a><br/> ";
                        }

                        return listFileName;
                    }
                },
                { data: 'memoCodeSequence', title: 'Memo Sequence', className: 'text-center' },
                {
                    data: 'status',
                    title: 'Status',
                    render: function(data, type, row) {
                        return row.status == true ? 'Enable' : 'Disable';
                    }
                }
            ]


        });
    }


    function loadDDL() {
        $(".ddlMemoType").empty();
        $(".ddlMemoType").append("<option value=''>Please select</option>");
        $.ajax({
            url: apiUrl + '/MasterData/GetMemoType',
            success: function(data) {
                $.each(data.data, function(i, val) {
                    $(".ddlMemoType").append("<option value=" + val.memoTypeID + ">" + val.memoTypeName + "</option>");
                });
            }
        });


        $('#ddlMemoCode').select2({
            placeholder: 'Please Select',
            cache: true,
            ajax: {
                url: apiUrl + '/MasterData/GetMemoCodeBy',
                dataType: 'json',
                type: 'GET',
                data: function(params) {
                    return {
                        param: params.term,
                        type: $("#ddlMemoType").val()
                    };
                },
                processResults({ data }) {
                    return {
                        results: $.map(data, function(item) {
                            return {
                                text: item.memoCode + ' - ' + item.description,
                                id: item.memoCodeID,
                            }
                        })
                    }
                }
            }
        });
    }

    //$('#selectallmemomapping').click(function () {
    //    var checked = $('#selectallmemomapping').prop('checked');
    //    $("input:checkbox[name=selectmemomapping]").each(function () {
    //        $(this).prop("checked", checked);
    //    });
    //});

    $('#selectalladdmemocode').click(function() {
        var checked = $('#selectalladdmemocode').prop('checked');
        $("input:checkbox[name=selectmemocode]").each(function() {
            $(this).prop("checked", checked);
        });
    });

    $('#ddlMemoType').on('change', function(e) {
        var status = $(this).val();
        dataTableMemoMapping.column(0).search(status).draw();
    });

    //$('#ddlMemoCode').on('change', function (e) {
    //    var status = $(this).val();
    //    dataTableMemoMapping.column(4).search(status).draw();
    //});

    $('#ddlMemoStatus').on('change', function(e) {
        var status = $(this).val();

        if (status == "true") {
            dataTableMemoMapping.column(8).search(true).draw();
        } else {
            dataTableMemoMapping.column(8).search(false).draw();
        }

    });

    $('#ddlMemoCode').on('change', function(e) {
        var status = $('#ddlMemoCode').val();
        dataTableMemoMapping.column(3).search(status).draw();
    });


    //--- Memo Type --//
    function enableDisbleMemoType(isDisable, isClear) {
        $('#txtMemoTypeName').prop('disabled', isDisable);
        $('#txtMemoTypeSequence').prop('disabled', isDisable);

        if (isClear) {
            $("#tableMemoTypeList tr input:radio[name=selectmemotype]").each(function() {
                $(this).prop("checked", false);
            });
            $('#txtMemoTypeID').val("");
            $('#txtMemoTypeName').val("");
            $('#txtMemoTypeSequence').val("");
        }
    }
    $("#showModalAddMemoType").click(function() {
        $("#actionType").val("");
        enableDisbleMemoType(true, true);

    });

    $("#btnAddMemoType").click(function() {
        $("#actionType").val("add");
        enableDisbleMemoType(false, true)
    });

    $("#btnEditMemoType").click(function() {
        if ($('#txtMemoTypeName').val().trim() != "") {
            $("#actionType").val("edit");
            enableDisbleMemoType(false, false);
        } else {
            alert("กรุณาเลือกข้อมูลที่ต้องการแก้ไข");
        }

    });

    $("#btnDeleteMemoType").click(function() {
        if ($('#txtMemoTypeName').val().trim() != "") {
            $("#actionType").val("delete");
            actionMemoType();
        } else {
            alert("กรุณาเลือกข้อมูลที่ต้องการลบ");
        }
    });

    $("#btnSaveMemoType").click(function() {

        actionMemoType();
    });

    //$('#tableMemoTypeList tbody').on('click', '.selectmemotype', function () {
    $(document).on('click', '.selectmemotype', function() {
        if (this.checked == true) {
            var data = dataTableMemoTypeList.row(this.closest('tr')).data();
            $('#txtMemoTypeID').val(data.memoTypeID);
            $('#txtMemoTypeName').val(data.memoTypeName);
            $('#txtMemoTypeSequence').val(data.memoTypeSequence);
        }
    });

    function actionMemoType() {
        var actionType = $("#actionType").val();
        var txtMemoTypeID = $('#txtMemoTypeID').val().trim();
        var txtMemoTypeName = $('#txtMemoTypeName').val().trim();
        var txtMemoTypeSequence = $('#txtMemoTypeSequence').val().trim();

        if (txtMemoTypeName != "" && txtMemoTypeSequence != "" && actionType != "") {
            var urlAction = "";
            if (actionType == "add" || actionType == "edit") {
                urlAction = apiUrl + '/MasterData/SaveMemoType';
            } else if (actionType == "delete") {
                urlAction = apiUrl + '/MasterData/DeleteMemoType';
            }

            var frmdata = {
                memoTypeID: txtMemoTypeID,
                memoTypeName: txtMemoTypeName,
                memoTypeSequence: txtMemoTypeSequence,
                userID: $("#userName").text()
            };

            $.ajax({
                type: "POST",
                url: urlAction,
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(frmdata),
                datatype: "json",
                success: function(data) {
                    if (data.success) {
                        alert(data.status.message)
                        dataTableMemoTypeList.ajax.reload();
                        enableDisbleMemoType(true, true);
                        $('#actionType').val("");
                        loadDDL();
                    } else if (!data.success) {
                        alert(data.status.message);
                    } else {
                        alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
                    }
                }
            });
        } else {
            alert('กรุณากรอกข้อมูลให้ครบถ้วน');
        }
    }
    //--- Memo Type --//

    //--- Memo Code --//
    function enableDisbleMemoCode(isDisable, isClear) {
        $('#ddlMemoCode_Type').prop('disabled', isDisable);
        $('#txtMemoCode_Sequence').prop('disabled', isDisable);
        $('#txtMemoCode_Code').prop('disabled', isDisable);
        $('#txtMemoCode_File').prop('disabled', isDisable);
        $('#txtMemoCode_Description').prop('disabled', isDisable);
        $('.radioMemoCode_Status').prop('disabled', isDisable);
        if (isDisable) {
            $('.removefile').hide();
        } else {
            $('.removefile').show();
        }

        if (isClear) {
            $("#tableMemoCodeList tr input:radio[name=selectmemocode]").each(function() {
                $(this).prop("checked", false);
            });
            $('#listFile').empty();
            $('#txtMemoCodeID').val("");
            $('#ddlMemoCode_Type').val("")
            $('#txtMemoCode_Sequence').val("");
            $('#txtMemoCode_Code').val("");
            $('#txtMemoCode_File').val("");
            $('#txtMemoCode_Description').val("");
            $('#radioMemoCode_StatusEnable').prop('checked', true);
            listFiles.items.clear();
        }
    }
    $("#showModalAddMemoCode").click(function() {
        $("#actionTypeMemocode").val("");
        enableDisbleMemoCode(true, true);
    });

    $("#btnAddMemoCode").click(function() {
        $("#actionTypeMemocode").val("add");
        enableDisbleMemoCode(false, true)
    });
    $("#btnEditMemocode").click(function() {
        if ($('#txtMemoCode_Sequence').val().trim() != "") {
            $("#actionTypeMemocode").val("edit");
            enableDisbleMemoCode(false, false);
        } else {
            alert("กรุณาเลือกข้อมูลที่ต้องการแก้ไข");
        }

    });

    $("#btnDeleteMemocode").click(function() {
        if ($('#txtMemoCode_Sequence').val().trim() != "") {
            $("#actionTypeMemocode").val("delete");
            var txtMemoCodeID = $('#txtMemoCodeID').val();
            var frmdata = {
                memoCodeID: txtMemoCodeID,
                userID: $("#userName").text()
            };

            $.ajax({
                type: "POST",
                url: apiUrl + '/MasterData/DeleteMemoCode',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(frmdata),
                datatype: "json",
                success: function(data) {
                    if (data.success) {
                        alert(data.status.message)
                        dataTableMemoCodeList.ajax.reload();
                        dataTableMemoMapping.ajax.reload();
                        enableDisbleMemoCode(true, true);
                        $('#actionTypeMemocode').val("");
                    } else if (!data.success) {
                        alert(data.status.message);
                    } else {
                        alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
                    }
                }
            });
        } else {
            alert("กรุณาเลือกข้อมูลที่ต้องการลบ");
        }
    });

    $("#btnSaveMemoCode").click(function() {
        actionMemoCode();
    });

    function actionMemoCode() {
        var actionTypeMemocode = $("#actionTypeMemocode").val();
        var txtMemoCodeID = $('#txtMemoCodeID').val();
        var ddlMemoCode_TypeID = $('#ddlMemoCode_Type').val();
        var ddlMemoCode_TypeName = $('#ddlMemoCode_Type option:selected').text();
        var txtMemoCode_Sequence = $('#txtMemoCode_Sequence').val().trim();
        var txtMemoCode_Code = $('#txtMemoCode_Code').val().trim();
        var txtMemoCode_Description = $('#txtMemoCode_Description').val().trim();
        var status = $('input[name="radioMemoCode_Status"]:checked').val();

        if (ddlMemoCode_Type != "" && txtMemoCode_Sequence != "" && txtMemoCode_Code != "" && txtMemoCode_Description != "" && actionTypeMemocode != "") {
            var urlAction = "";
            if (actionTypeMemocode == "add" || actionTypeMemocode == "edit") {
                urlAction = apiUrl + '/MasterData/SaveMemoCode';
            } else if (actionTypeMemocode == "delete") {
                urlAction = apiUrl + '/MasterData/DeleteMemoCode';
            }

            var formData = new FormData();

            var files = document.getElementById("txtMemoCode_File").files;
            for (var i = 0; i < files.length; i++) {
                // formData.append('attachments', files[i]);
                listFiles.items.add(files[i]);
            }
            for (var i = 0; i < listFiles.files.length; i++) {
                formData.append('attachments', listFiles.files[i]);
            }
            if (listFiles.files.length == 0) {
                formData.append('attachments', null);
            }
            formData.append("memoCodeID", txtMemoCodeID == "" ? 0 : txtMemoCodeID);
            formData.append("memoCode", txtMemoCode_Code);
            formData.append("memoCodeSequence", txtMemoCode_Sequence);
            formData.append("memoTypeID", ddlMemoCode_TypeID);
            formData.append("memoType", ddlMemoCode_TypeName);
            formData.append("description", txtMemoCode_Description);
            formData.append("status", status);
            formData.append("userID", $("#userName").text());
            console.log(formData);
            $.ajax({
                type: "POST",
                url: urlAction,
                processData: false,
                contentType: false,
                data: formData,
                datatype: "json",
                success: function(data) {
                    if (data.success) {
                        alert(data.status.message)
                        dataTableMemoCodeList.ajax.reload();
                        dataTableMemoMapping.ajax.reload();
                        enableDisbleMemoCode(true, true);
                        $('#actionTypeMemocode').val("");
                        loadDDL();
                    } else if (!data.success) {
                        alert(data.status.message);
                    } else {
                        alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
                    }
                }
            });
        } else {
            alert('กรุณากรอกข้อมูลให้ครบถ้วน');
        }
    }



    /* $('#tableMemoCodeList tbody').on('click', '.selectmemocode', function () {*/
    $(document).on('click', '.selectmemocode', function() {
        if (this.checked == true) {
            listFiles.items.clear();
            var data = dataTableMemoCodeList.row(this.closest('tr')).data();

            $('#txtMemoCodeID').val(data.memoCodeID);
            $("#ddlMemoCode_Type option").each(function() {
                if ($(this).text() == data.memoTypeName) {
                    $(this).attr('selected', true);
                } else {
                    $(this).attr('selected', false);
                }
            });
            $('#txtMemoCode_Sequence').val(data.memoCodeSequence);
            $('#txtMemoCode_Code').val(data.memoCode);
            $('#txtMemoCode_Description').val(data.description);
            $('#txtMemoCode_Sequence').val(data.memoCodeSequence);
            if (data.status) {
                $('#radioMemoCode_StatusEnable').prop("checked", true)
            } else {
                $('#radioMemoCode_StatusDisable').prop("checked", true)
            }

            var listFileName = "";
            for (var i = 0; i < data.fileAttachment.length; i++) {
                listFileName += "<a href='" + data.fileAttachment[i].filePath + "' target='_blank'>" + data.fileAttachment[i].fileName + "</a><span onclick='removefile(\"" + data.fileAttachment[i].fileName + "\")'><i class='fas fa-trash removefile ml-2' style='color:red;cursor:pointer'></i></span><br/> ";
                (async() => {
                    createFile(data.fileAttachment[i].fileName, data.fileAttachment[i].filePath).then(function(resultFile) {
                        listFiles.items.add(resultFile);
                    });
                })()
            }
            $('#listFile').empty();
            $('#listFile').append(listFileName);
            let isdis = $('#ddlMemoCode_Type').prop('disabled');
            if (isdis) {
                $('.removefile').hide();
            } else {
                $('.removefile').show();
            }

        }
    });

    $("#btnResetMain").click(function() {
        $('#ddlMemoType').val("");
        $('#ddlMemoCode').val("").trigger("change");
        $('#ddlMemoStatus').val("");
        loadMemoMapping();
    });
    //--- Memo Code --//


});