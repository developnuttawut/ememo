function clearInput() {
    $('.datepicker').val("");
    $("select").val($("select option:first").val()).trigger('change');

    $('#divSearchResult div.card-body').html('');
    $('#divSearchResult').addClass('d-none');

    $('#ddlResendNeed').val('').trigger('change')
}

function isValidDate(d) {
    return d instanceof Date && !isNaN(d);
}

// function buttonLoading(id, isLoading, defaultText) {
//     if (isLoading) {
//         $('#' + id + '').html(`
//         <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
//          Loading...
//         `);
//     } else {
//         if (defaultText == null) {
//             defaultText = '';
//         }
//         $('#' + id + '').html(defaultText);
//     }
//     $('#' + id + '').prop("disabled", isLoading);
// }

function searchReport(btnid, frmdata, previewMethod, exportMethod) {
    var btnHtmlBefore = $('#' + btnid + '').html();
    buttonLoading(btnid, true);
    $.ajax({
        type: "POST",
        url: apiUrl + '/Report/' + previewMethod,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(frmdata),
        datatype: "json",
        success: function(data) {
            buttonLoading(btnid, false, btnHtmlBefore);
            if (data.status.code == 200) {
                displayData(data.htmlPreview);
            } else if (data.status.code == 404) {
                alert('ไม่พบข้อมูล');
            } else {
                alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
            }
        },
        error: function() {
            alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
            buttonLoading(btnid, false, btnHtmlBefore);
        }
    });
    $('#btnExportExcel').on('click', function() {
        window.location.href = apiUrl + '/Report/' + exportMethod + '?' + $.param(frmdata);
    });
}

function displayData(htmlData) {
    $('#divSearchResult div.card-body').html(htmlData);
    $('#divSearchResult').removeClass('d-none');

    $('#btnPrint').on('click', function() {
        var newWindow = window.open();
        newWindow.document.write('<html><head><title></title>');
        newWindow.document.write(`
        <style>
        @media print {
            @page {
                size:A4 landscape;
            }
            body {
                zoom: 70%;
            }
        }
        </style>
      `);
        newWindow.document.write('</head><body >');
        newWindow.document.write($('#divSearchResult div.card-body').html());
        newWindow.document.write('</body></html>');
        newWindow.document.close();
        newWindow.print();
    });
}

function init_ddl_productCategory() {
    $('#ddlProductCategory').empty().trigger('change');
    $.ajax({
        url: apiUrl + '/MasterData/GetProductCategoryReport',
        dataType: 'json',
        type: 'GET'
    }).done(function(data) {
        var datasource = $.map(data.data, function(item) {
            return {
                text: item.description,
                id: item.productCategoryID,
            }
        });
        datasource.unshift({
            "text": "All",
            "id": ""
        })
        $('#ddlProductCategory').select2({
            data: datasource,
            minimumResultsForSearch: -1
        });

        $('#ddlProductCategory').val('').trigger('change');
    });

}