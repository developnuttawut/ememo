$(document).ready(function() {
    initDatepicker();
    init_ddl_productCategory();
    $('#btnPendingSummaryPreview').click(function() {
        var submissionDateFrom = convertThaiToUS($('#submissionDateFrom').val());
        var submissionDateTo = convertThaiToUS($('#submissionDateTo').val());
        if (isValidDate(submissionDateFrom)) {
            if (isValidDate(submissionDateTo)) {
                //กรณีระบุวันที่สิ้นสุด ตรวจสอบวันที่เริ่มต้นมากกว่าวันที่สิ้นสุด
                if (submissionDateFrom.getTime() > submissionDateTo.getTime()) {
                    alert('กรุณาระบุวันที่ที่ต้องการค้นหาให้ถูกต้อง')
                } else {
                    //Search
                    prepareData(submissionDateFrom, submissionDateTo);
                }
            } else {
                //ถ้าระบุเงื่อนไขในส่วนวันที่เริ่มต้นแต่ไม่ระบุวันที่สิ้นสุด ระบบจะแสดงข้อมูลสำหรับวันที่ผู้ใช้งานระบุเท่านั้น
                //search
                prepareData(submissionDateFrom, submissionDateTo);
            }
        } else {
            alert('กรุณาระบุวันที่ต้องการค้นหาเริ่มต้น')
        }
    });

    $('#btnClear').on('click', function() {
        clearInput();
    });
});

function prepareData(submissionDateFrom, submissionDateTo) {
    var frmdata = {
        submissionDateFrom: submissionDateFrom.toJSON(),
        submissionDateTo: submissionDateTo.toJSON(),
        productCategory: $('#ddlProductCategory').val()
    };
    searchReport('btnPendingSummaryPreview', frmdata, 'PreviewPendingSummaryReport', 'ExportPendingSummaryReport');
}