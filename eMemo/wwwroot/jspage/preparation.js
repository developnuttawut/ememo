﻿var waitingForProcessId;
$(document).ready(function() {
    initDatepicker();
    $('#txtDateForm').val(moment().add(543, 'year').format('DD/MM/YYYY'));
    initModalUnit();
    initModalGroup();
    // DDLEBoaStatus();
    initEBoaStatus();
    // DDLDeliveryStatus();
    initDeliveryStatus();
    checkRole();
});

$('#linkRetrieve').click(function() {
    $.ajax({
        type: "GET",
        url: apiUrl + '/Transaction/RetrieveTransaction',
        success: function(data) {
            if (data.success) {
                alert(data.status.message)
            } else if (!data.success) {
                alert(data.status.message);
            } else {
                alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
            }
        }
    });
});

function checkRole() {
    if (localStorage.getItem('isAdministrator') == 'true') {
        //$('#linkRetrieve').css({ pointerEvents: "none" });
        //$('#btnAdhocSend').css({ pointerEvents: "none" });
        //$('#btnSaveStatus').css({ pointerEvents: "none" });

    }
}

function initModalUnit() {
    var tableUnit = $('#tblUnit').DataTable({
        responsive: true,
        autoWidth: false,
        ajax: {
            url: apiUrl + '/MasterData/GetAgentUnit',
            dataSrc: "data",
        },
        columns: [
            { data: null, defaultContent: '<button class="btn btn-primary btn-xs" type="button">Select</button>', orderable: false },
            { data: 'agentUnitCode', title: 'Unit Code' },
            { data: 'agentUnitName', title: 'Unit Name' }
        ],
    });
    $('#tblUnit tbody').on('click', 'button', function() {
        var data = tableUnit.row($(this).parents('tr')).data();
        $('#txtAgentUnit').val(data.agentUnitName);
        $('#modalUnit').modal('hide');
    });
}

function initModalGroup() {
    var tableGroup = $('#tblGroup').DataTable({
        responsive: true,
        autoWidth: false,
        ajax: {
            url: apiUrl + '/MasterData/GetAgentGroup',
            dataSrc: "data",
        },
        columns: [
            { data: null, defaultContent: '<button class="btn btn-primary btn-xs" type="button">Select</button>', orderable: false },
            { data: 'agentGroupCode', title: 'Group Code' },
            { data: 'agentGroupName', title: 'Group Name' }
        ],
    });
    $('#tblGroup tbody').on('click', 'button', function() {
        var data = tableGroup.row($(this).parents('tr')).data();
        $('#txtAgentGroup').val(data.agentGroupName);
        $('#modalGroup').modal('hide');
    });
}


$("#btnSearchTransaction").click(function() {
    var txtDateForm = convertThaiToUS($("#txtDateForm").val());
    var txtDateTo = convertThaiToUS($("#txtDateTo").val());
    var txtPolicyNo = $("#txtPolicyNo").val();
    var txtProposalNo = $("#txtProposalNo").val();
    var txtDocNo = $("#txtDocNo").val();
    var ddlMemoStatus = $('#ddlMemoStatus').val();
    var ddlDeliveryStatus = $("#ddlDeliveryStatus").val();
    var ddlDeliveryRound = $("#ddlDeliveryRound").val();
    var txtID = $("#txtID").val();
    var txtPartyID = $("#txtPartyID").val();
    var txtCustomerName = $("#txtCustomerName").val();
    var txtCustomerSurname = $("#txtCustomerSurname").val();
    var txtAgentCode = $("#txtAgentCode").val();
    var txtAgentName = $("#txtAgentName").val();
    var txtAgentSurname = $("#txtAgentSurname").val();
    var txtAgentUnit = $("#txtAgentUnit").val();
    var txtAgentGroup = $("#txtAgentGroup").val();

    if (txtDateForm == '' && txtDateTo == '' && txtPolicyNo == '' && txtProposalNo == '' && txtDocNo == '' && ddlMemoStatus == '' && ddlDeliveryStatus == '' && ddlDeliveryRound == '' && txtID == '' && txtPartyID == '' && txtCustomerName == '' && txtCustomerSurname == '' && txtAgentCode == '' && txtAgentName == '' && txtAgentSurname == '' && txtAgentUnit == '' && txtAgentGroup == '') {
        alert('กรุณาระบุข้อมูลเพื่อกดค้นหา');
    } else {

        var frmdata = {
            documentNo: txtDocNo,
            transactionDateForm: txtDateForm,
            transactionDateTo: txtDateTo,
            policyNo: txtPolicyNo,
            proposalNo: txtProposalNo,
            memoStatus: ddlMemoStatus,
            deliveryStatus: ddlDeliveryStatus,
            deliveryRound: ddlDeliveryRound,
            citizenID: txtID,
            partyID: txtPartyID,
            customerName: txtCustomerName,
            customerSurname: txtCustomerSurname,
            agentCode: txtAgentCode,
            agentName: txtAgentName,
            agentSurname: txtAgentSurname,
            agentUnit: txtAgentUnit,
            gentGroup: txtAgentGroup
        };


        $.ajax({
            type: "POST",
            url: apiUrl + '/Transaction/SearchMemoTransaction',
            //url: 'https://localhost:44312/api/preparation/',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(frmdata),
            datatype: "json",
            success: OnSuccess
        });
    }
});

$("#btnResetMain").click(function() {
    $("#txtDateForm").val("");
    $("#txtDateTo").val("");
    $("#txtPolicyNo").val("");
    $("#txtProposalNo").val("");
    $("#txtDocNo").val("");
    $('#ddlMemoStatus').val("");
    $("#ddlDeliveryStatus").val(waitingForProcessId).trigger('change');
    $("#ddlDeliveryRound").val("1")
    $("#txtID").val("");
    $("#txtPartyID").val("");
    $("#txtCustomerName").val("");
    $("#txtCustomerSurname").val("");
    $("#txtAgentCode").val("");
    $("#txtAgentName").val("");
    $("#txtAgentSurname").val("");
    $("#txtAgentUnit").val("");
    $("#txtAgentGroup").val("");

    $('#tblDetails').dataTable().fnClearTable();
    $('#rowtotal').html(0);
});

function OnSuccess(response) {
    $.ajax({
        url: apiUrl + '/Transaction/GetDeliveryStatus',
        dataType: 'json',
        type: 'GET'
    }).done(function(data) {
        let invalidEntries = 0
        let arr = data.data.filter(function(item) {
            if (item.description.match(/Hold|Cancelled|Waiting for process|Unmapping/i) != null) {
                return true
            }
            invalidEntries++
            return false;
        })
        $('#rowtotal').html(response.data.length);
        $('#tblDetails').DataTable({
            data: response.data,
            "scrollX": true,
            "sScrollXInner": "100%",
            columns: [{
                    targets: 0,
                    searchable: false,
                    orderable: false,
                    stateSave: true,
                    className: 'dt-body-center',
                render: function (data, type, full, meta) {
                        return '<input type="checkbox" name="selectPreparation" value="' + $('<div/>').text(data).html() + '">';
                    }
                },
                {
                    data: 'transactionID',
                    visible: false
                },
                {
                    data: 'transactionDate',
                    title: 'Transaction Date',
                    className: 'text-center',
                    render: function(data) {
                        return moment(data).add(543, 'year').format('DD/MM/YYYY');
                    }
                },
                {
                    data: 'documentNo',
                    title: 'eBao Document No.',
                    className: 'text-center',
                    render: function(data, type, row) {
                        return '<a href="/eMemo/Preparation/Information?id=' + row['transactionID'] + '">' + data + '</a>';
                    },
                },
                { data: 'policyNo', title: 'Policy No.', className: 'text-center' },
                { data: 'proposalNo', title: 'Proposal No.', className: 'text-center' },
                { data: 'policyName', title: 'Policy Name' },
                { data: 'agentCode', title: 'Agent Code', className: 'text-center' },
                { data: 'agentName', title: 'Agent Name' },
                { data: 'agentUnit', title: 'Unit' },
                { data: 'agentGroup', title: 'Group' },
                { data: 'eBaoMemoStatus', title: 'eBao Memo Status', className: 'text-center' },
                { data: 'round', title: 'Round', className: 'text-center' },
                {
                    data: 'estimateTime',
                    title: 'Estimate time will be send',
                    className: 'text-center',
                    render: function(data) {
                        return moment(data).add(543, 'year').format('DD/MM/YYYY HH:mm');
                    }
                },
                {
                    data: 'pdfFile',
                    title: 'PDF File',
                    className: 'text-center',
                    render: function(data, type, row) {
                        return '<a href="' + apiUrl + '/FileAttachment/PreviewMemoAttachment/' + row['documentNo'] + '" target="_blank"><ion-icon style="color:red;font-size:25px;cursor:pointer" name="document-attach"></ion-icon></a>';
                    }
                },
                {
                    data: 'deliveryStatus',
                    title: 'Delivery Status',
                    render: function(data, type, row, meta) {
                        var $select = $("<select class='custom-select custom-select-sm' id='ddlStatus_" + meta.row + "' ></select>", {});
                        $.each(arr, function(k, v) {
                            var $option = $("<option></option>", {
                                "text": v.description,
                                "value": v.deliveryStatusID
                            });
                            if (data === v.description) {
                                $option.attr("selected", "selected")
                            }
                            $select.append($option);
                        });
                        return $select.prop("outerHTML");
                    }
                }
            ],
            bDestroy: true
        });
    });
};

$("#btnSaveStatus").click(function() {
    var table = $('#tblDetails').DataTable();

    var statusUpdate = [];
    var allDropDowns = table.$('select');
    $.each(allDropDowns, function(index, dropdown) {
        statusUpdate.push(dropdown.value);
    });
    var data = table
        .rows()
        .data()
        .toArray();

    var frmdata = [];
    let i = 0;
    for (let key in data) {
        var d = data[key];
        frmdata.push({
            transactionID: d["transactionID"],
            deliveryStatus: statusUpdate[i]
        })
        i++
    }

    var request = {
        "userID": $("#userName").text(),
        "updateData": frmdata
    }
    // console.log(frmdata)

    $.ajax({
        type: "POST",
        url: apiUrl + '/Transaction/UpdateDeliveryStatus',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        datatype: "json",
        success: function(data) {
            if (data.success) {
                alert(data.status.message)
            } else if (!data.success) {
                alert(data.status.message);
            } else {
                alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
            }
        }
    });

});

$('#selectallpreparation').click(function() {
    var checked = $('#selectallpreparation').prop('checked');
    $("input:checkbox[name=selectPreparation]").each(function() {
        $(this).prop("checked", checked);
    });
});

$("#btnResetDetail").click(function() {});

// function SearchValue() {
//var InquiryModel = {
//    "documentNo": $("#documentNo").val(),
//    "transactionDateForm": $("#transactionDateForm").val(),
//    "transactionDateTo": $("#transactionDateTo").val(),
//    "policyNo": $("#policyNo").val(),
//    "proposalNo": $("#proposalNo").val(),
//    "ebaoMemoStatusID": $("#ebaoMemoStatusID").val(),
//    "osMemo": $("#osMemo").val(),
//    "sending": $("#sending").val(),
//    "citizenID": $("#citizenID").val(),
//    "partyID": $("#partyID").val(),
//    "customerName": $("#customerName").val(),
//    "customerSurname": $("#customerSurname").val(),
//    "agentCode": $("#agentCode").val(),
//    "agentName": $("#agentName").val(),
//    "agentSurname": $("#agentSurname").val(),
//    "agentUnit": $("#agentUnit").val(),
//    "agentGroup": $("#agentGroup").val(),
//}

//$.ajax({
//    type: "POST",
//    //url: apiUrl + '/Transaction/SearchMemoTransaction',
//    url: 'https://localhost:44312/api/preparation/',
//    contentType: "application/json; charset=utf-8",
//    data: JSON.stringify(InquiryModel),
//    datatype: "json",
//    success: function (data) {
//        console.log(data)
//    },
//    error: function (err) {
//        console.error(err)
//    }
//});
// }

// function ClearValue() {
//     $("#documentNo").val("");
//     $("#transactionDateForm").val("");
//     $("#transactionDateTo").val("");
//     $("#policyNo").val("");
//     $("#proposalNo").val("");
//     $("#ebaoMemoStatusID").val("12");
//     $("#osMemo").val("12");
//     $("#sending").val("12");
//     $("#citizenID").val("");
//     $("#partyID").val("");
//     $("#customerName").val("");
//     $("#customerSurname").val("");
//     $("#agentCode").val("");
//     $("#agentName").val("");
//     $("#agentSurname").val("");
//     $("#agentUnit").val("");
//     $("#agentGroup").val("");
// }
function initEBoaStatus() {
    $.ajax({
        url: apiUrl + '/Transaction/GetEBaoStatus',
        dataType: 'json',
        type: 'GET'
    }).done(function(data) {
        let invalidEntries = 0
        let arr = data.data.filter(function(item) {
            if (item.description.match(/Printed/i) != null) {
                return true
            }
            invalidEntries++
            return false;
        })
        var dataSource = $.map(arr, function(item) {
            return {
                text: item.description,
                id: item.eBaoStatusID,
            }
        });
        $('#ddlMemoStatus').select2({
            data: dataSource,
            minimumResultsForSearch: -1
        });
    });
}

function initDeliveryStatus() {
    $.ajax({
        url: apiUrl + '/Transaction/GetDeliveryStatus',
        dataType: 'json',
        type: 'GET'
    }).done(function(data) {
        let invalidEntries = 0
        let arr = data.data.filter(function(item) {
            if (item.description.match(/Waiting for process|Hold|Cancelled|Unmapping/i) != null) {
                return true
            }
            invalidEntries++
            return false;
        })
        var dataSource = $.map(arr, function(item) {
            return {
                text: item.description,
                id: item.deliveryStatusID,
            }
        });
        var defaultValue = dataSource.filter(w => w.text.toLowerCase() == 'waiting for process');
        waitingForProcessId = defaultValue[0].id;
        $('#ddlDeliveryStatus').select2({
            data: dataSource,
            minimumResultsForSearch: -1
        });
        $('#ddlDeliveryStatus').val(waitingForProcessId).trigger('change');
    });
}
// function DDLEBoaStatus() {
//     $(".ddlMemoStatus").empty();
//     $.ajax({
//         //url: 'https://localhost:44312/api/Master/1',
//         url: apiUrl + '/Transaction/GetEBaoStatus',
//         success: function(data) {
//             $.each(data.data, function(i, val) {
//                 if (val.description.match(/Printed/i) != null) {
//                     $(".ddlMemoStatus").append("<option value='" + val.eBaoStatusID + "' selected>" + val.description + "</option>");
//                 }                
//             });
//         }
//     });
// }

// function DDLDeliveryStatus() {
//     $(".ddlDeliveryStatus").empty();
//     $(".ddlDeliveryStatus").append("<option value=''>Please select</option>");
//     $.ajax({
//         //url: 'https://localhost:44312/api/Master/1',
//         url: apiUrl + '/Transaction/GetDeliveryStatus',
//         success: function(data) {
//             $.each(data.data, function(i, val) {
//                 $(".ddlDeliveryStatus").append("<option value='" + val.deliveryStatusID + "'>" + val.description + "</option>");
//             });
//         }
//     });

// }
$('#btnAdhocSend').click(function () {
    var btnHtmlBefore = $('#btnAdhocSend').html();
    buttonLoading('btnAdhocSend', true);
    var frmdata = {
        "userID": $("#userName").text(),
        "transactionIDList": []
    };
    var table = $('#tblDetails').DataTable();

    var allCheckbox = table.$('input:checkbox');
    $.each(allCheckbox, function(index, chx) {
        if (chx.checked) {
            frmdata.transactionIDList.push(table.row(index).data()["transactionID"]);
        }
    });

    $.ajax({
        type: "POST",
        url: apiUrl + '/Transaction/AdhocSendMemo',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(frmdata),
        datatype: "json",
        success: function(data) {
            if (data.success) {
                alert(data.status.message)
            } else if (!data.success) {
                alert(data.status.message);
            } else {
                alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
            }

            buttonLoading('btnAdhocSend', false, btnHtmlBefore);
        },
        error: function () {
            alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
            buttonLoading('btnAdhocSend', false, btnHtmlBefore);
        }
    });
});