﻿var dataTableRuleManagement;
$(document).ready(function () {
    loadRuleMapping();

    initChannelDdl();
    initSubChannelDdl();
    initSubChannelDdlMod();
    initSOBDdl();
    initBusinessUnitDdl();
    initProductCategoryDdl();

    $.ajax({
        url: apiUrl + '/MasterData/GetSaleGroup',
        dataType: 'json',
        type: 'GET',
        success: function (result) {
            populateSelect('checkSendEmailToSaleSupport', result.data, 'string', 'string');
        }
    });
    $.ajax({
        url: apiUrl + '/MasterData/GetUnderwritingGroup',
        dataType: 'json',
        type: 'GET',
        success: function (result) {
            populateSelect('checkSendEmailToSaleUnderwriting', result.data, 'string', 'string');
        }
    });

    loadProductCategoryAll();
    var ProductCategoryAll = [];

    checkRole();

    function checkRole() {
        if (localStorage.getItem('isAdministrator') == 'true') {
            $('#btnAddRule').css({ pointerEvents: "none" });
            $('#btnModifyRule').css({ pointerEvents: "none" });
            $('#btnDeleteDocumentChannel').css({ pointerEvents: "none" });
        }
    }

    function populateSelect(ddlId, data, idType, nameType) {
        if (!data || !data[0]) {
            return;
        }

        var select = document.getElementById(ddlId);
        var keys = Object.keys(data[0]);
        var idKey = typeof (parseInt(keys[0])) == idType ? keys[0] : keys[1];
        var nameKey = typeof (parseInt(keys[0])) == nameType ? keys[0] : keys[1];

        for (var i = 0; i < data.length; i++) {
            var option = document.createElement('option');
            option.value = data[i][idKey];
            option.label = data[i][nameKey];
            select.appendChild(option);
        }
        $("#" + ddlId).prepend("<option value='' selected='selected'>Please Select</option>");
    }



    function loadProductCategoryAll() {
        $.ajax({
            url: apiUrl + '/MasterData/GetProductCategory',
            success: function (data) {
                ProductCategoryAll = data.data;
            }
        });

    }

    function loadRuleMapping() {
        if (dataTableRuleManagement !== null) {
            $('#tableRuleManagement').DataTable().destroy();
            dataTableRuleManagement = null;
            $('#tableRuleManagement').empty();
        }
        dataTableRuleManagement = $('#tableRuleManagement').DataTable({
            "scrollX": true,
            "sScrollXInner": "100%",
            "ajax": {
                type: "GET",
                url: apiUrl + '/MasterData/GetRuleManagement',
                contentType: "application/json; charset=utf-8",
                dataSrc: function (result) {
                    return result.data;
                }
            },
            columns: [{
                title: 'Use',
                targets: 0,
                searchable: false,
                orderable: false,
                className: 'text-center',
                render: function (data, type, full, meta) {
                    return '<input type="radio" name="selectrule" class="selectrule" data-wasChecked="false" />';
                }
            },
            { data: "ruleManagementID", visible: false },
            { data: 'channel', title: 'Channel', className: 'text-center' },
            { data: "subChannelID", visible: false },
            { data: "subChannel", visible: false },
            {
                data: 'subChannel',
                title: 'Sub Channel',
                render: function (data, type, full, meta) {

                    return data != "" && data != null ? data : "Not Applicate";
                },
                className: 'text-center'
            },
            { data: "sobID", visible: false },
            { data: "sob", visible: false },
            {
                data: 'sob',
                title: 'Source Of Business',
                render: function (data, type, full, meta) {
                    return data != "" && data != null ? data : "Not Applicate";
                },
                className: 'text-center'
            },
            { data: "businessUnitID", visible: false },
            { data: "businessUnit", visible: false },
            {
                data: 'businessUnit',
                title: 'Business Unit',
                render: function (data, type, full, meta) {
                    return data != "" && data != null ? data : "Not Applicate";
                },
                className: 'text-center'
            },
            { data: "productCategory", visible: false },
            {
                data: 'productCategory',
                title: 'Product Category',
                render: function (data, type, full, meta) {
                    var productCat = "";
                    for (var i = 0; i < data.length; i++) {
                        productCat += ", " + data[i]
                    }
                    return data.length == 0 ? "Not Applicate" : productCat.substring(1);;
                },
                className: 'text-center'
            },
            { data: 'firstTier', title: 'First Tier', className: 'text-center' },
            { data: 'secondTier', visible: false },
            {
                data: 'secondTier',
                title: 'Second Tier',
                className: 'text-center',
                render: function (data, type, full, meta) {
                    return data != "" && data != null ? data : "Not Applicate";
                },
                className: 'text-center'
            },
            { data: 'sendEmailToSaleSupport', title: 'Sale Support', className: 'text-center' },
            { data: "status", visible: false },
            {
                data: 'status',
                title: 'Status',
                className: 'text-center',
                render: function (data, type, full, meta) {
                    return data ? "Enable" : "Disable";
                },
                className: 'text-center'
            }
            ]

        });
    }


    // $('.selectlist').selectize({
    // });

    $('body').on('click', '.list-group .list-group-item', function () {
        $(this).toggleClass('active');
    });
    $('.list-arrows button').click(function () {
        var $button = $(this),
            actives = '';
        if ($button.hasClass('move-left')) {
            actives = $('.list-right ul li.active');
            actives.clone().appendTo('.list-left ul');
            actives.remove();
            $('.list-left ul li.active').removeClass('active');
        } else if ($button.hasClass('move-right')) {
            actives = $('.list-left ul li.active');
            actives.clone().appendTo('.list-right ul');
            actives.removeClass('active');
            actives.remove();
            $('.list-right ul li.active').removeClass('active');
        }

    });
    $('.dual-list .selector').click(function () {
        var $checkBox = $(this);
        if (!$checkBox.hasClass('selected')) {
            $checkBox.addClass('selected').closest('.well').find('ul li:not(.active)').addClass('active');
            $checkBox.children('i').removeClass('glyphicon-unchecked').addClass('glyphicon-check');
        } else {
            $checkBox.removeClass('selected').closest('.well').find('ul li.active').removeClass('active');
            $checkBox.children('i').removeClass('glyphicon-check').addClass('glyphicon-unchecked');
        }
    });
    $('[name="SearchDualList"]').keyup(function (e) {
        var code = e.keyCode || e.which;
        if (code == '9') return;
        if (code == '27') $(this).val(null);
        var $rows = $(this).closest('.dual-list').find('.list-group li');
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
        $rows.show().filter(function () {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });


    $("#btnSave").click(function () {
        actionRuleManagement();
    });
    $("#btnAddRule").click(function () {
        if ($('#txtRuleManagementID').val() == "") {
            clearRuleManagement();
            $("#modalAddRuleLongTitle").text("Add Rule Infomation");
            $("#actionTypeRuleManagement").val('add');
            $('#modalAddRule').modal('show');
            var listProductCategoryAll = "";
            for (var i = 0; i < ProductCategoryAll.length; i++) {
                listProductCategoryAll += "<li class='list-group-item'>" + ProductCategoryAll[i].description + "</li>"

            }
            $('#list-left').empty();
            $('#list-left').append(listProductCategoryAll);
        } else {
            alert("กรุณานำ checkbox ที่เลือกข้อมูลออกก่อน");
        }
    });

    $("#btnModifyRule").click(function () {
        if ($('#txtRuleManagementID').val() != "") {
            $("#actionTypeRuleManagement").val("edit");
            $("#modalAddRuleLongTitle").text("Edit Rule Infomation");
            $('#modalAddRule').modal('show');
        } else {
            alert("กรุณาเลือกข้อมูลที่ต้องการแก้ไข");
        }
    });
    $('#btnReset').click(function () {
        // let data = dataTableRuleManagement.rows('input:radio.selectrule');
        // console.log(data);

        dataTableRuleManagement.rows().every(function (rowIdx, tableLoop, rowLoop) {
            let data = this.data();
            if (data.ruleManagementID == $('#txtRuleManagementID').val()) {
                console.log(data);
                $('#ddlChannelModify').val($('#ddlChannelModify').find("option:contains('" + data.channel + "')").val()).trigger("change");
                bindSubChannelDdlMod(data.subChannelID);
                bindSOBDdlMod(data.sobID);
                bindBusinessUnitDdlMod(data.businessUnitID);

                $('#ddlFirstTier').val(data.firstTier);
                $('#ddlSecondTier').val(data.secondTier);

                if (data.firstTierChannel == "SMS") {
                    $('#radioFirstTierChannel').prop("checked", true)
                } else if (data.firstTierChannel == "E-Mail") {
                    $('#radioFirstTierChannelEmail').prop("checked", true)
                } else if (data.firstTierChannel == "SMS&E-Mail") {
                    $('#radioFirstTierChannelSMSEmail').prop("checked", true)
                }

                if (data.secondTierChannel == "SMS") {
                    $('#radioSecondTierChannel').prop("checked", true)
                } else if (data.secondTierChannel == "E-Mail") {
                    $('#radioSecondTierChannelEmail').prop("checked", true)
                } else if (data.secondTierChannel == "SMS&E-Mail") {
                    $('#radioSecondTierChannelSMSEmail').prop("checked", true)
                }


                $('#checkSendEmailToSaleSupport').val(data.sendEmailToSaleSupport);
                $('#checkSendEmailToSaleUnderwriting').val(data.sendEmailToSaleUnderwriting);

                $('#txtSpaceWorkingDays').val(data.spaceWorkingDays);

                var proAll = ProductCategoryAll;
                var proSelected = data.productCategory;

                var proNotSelected = proAll.filter(function (obj) { return proSelected.indexOf(obj.productCategoryID) == -1; });
                var optionProNotSelected = "";
                for (var i = 0; i < proNotSelected.length; i++) {
                    optionProNotSelected += "<li class='list-group-item'>" + proNotSelected[i].description + "</li>";
                }
                $('#list-left').empty();
                $('#list-left').append(optionProNotSelected);


                var optionProSelected = "";
                for (var i = 0; i < proSelected.length; i++) {
                    optionProSelected += "<li class='list-group-item'>" + proSelected[i] + "</li>";
                }
                $('#list-right').empty();
                $('#list-right').append(optionProSelected);
            }
        });
    });

    //$('#tableRuleManagement tbody').on('click', '.selectrule', function () {
    $(document).on('click', '.selectrule', function () {
        var wasChecked = $(this).attr('data-wasChecked');
        if (wasChecked == "true") {
            this.checked = !this.checked;
            $(this).attr('data-wasChecked', false);
            clearRuleManagement();
        } else {
            $(this).attr('data-wasChecked', true);
        }

        if (this.checked == true) {
            var data = dataTableRuleManagement.row(this.closest('tr')).data();
            $('#txtRuleManagementID').val(data.ruleManagementID);

            $('#ddlChannelModify').val($('#ddlChannelModify').find("option:contains('" + data.channel + "')").val()).trigger("change");

            bindSubChannelDdlMod(data.subChannelID);

            bindSOBDdlMod(data.sobID);

            bindBusinessUnitDdlMod(data.businessUnitID);

            $('#ddlFirstTier').val(data.firstTier);
            $('#ddlSecondTier').val(data.secondTier);

            if (data.firstTierChannel == "SMS") {
                $('#radioFirstTierChannel').prop("checked", true)
            } else if (data.firstTierChannel == "E-Mail") {
                $('#radioFirstTierChannelEmail').prop("checked", true)
            } else if (data.firstTierChannel == "SMS&E-Mail") {
                $('#radioFirstTierChannelSMSEmail').prop("checked", true)
            }

            if (data.secondTierChannel == "SMS") {
                $('#radioSecondTierChannel').prop("checked", true)
            } else if (data.secondTierChannel == "E-Mail") {
                $('#radioSecondTierChannelEmail').prop("checked", true)
            } else if (data.secondTierChannel == "SMS&E-Mail") {
                $('#radioSecondTierChannelSMSEmail').prop("checked", true)
            }

            $('#checkSendEmailToSaleSupport').val(data.sendEmailToSaleSupport);
            $('#checkSendEmailToSaleUnderwriting').val(data.sendEmailToSaleUnderwriting);

            $('#txtSpaceWorkingDays').val(data.spaceWorkingDays);

            var proAll = ProductCategoryAll;
            var proSelected = data.productCategory;

            var proNotSelected = proAll.filter(function (obj) { return proSelected.indexOf(obj.productCategoryID) == -1; });
            var optionProNotSelected = "";
            for (var i = 0; i < proNotSelected.length; i++) {
                optionProNotSelected += "<li class='list-group-item'>" + proNotSelected[i].description + "</li>";
            }
            $('#list-left').empty();
            $('#list-left').append(optionProNotSelected);


            var optionProSelected = "";
            for (var i = 0; i < proSelected.length; i++) {
                optionProSelected += "<li class='list-group-item'>" + proSelected[i] + "</li>";
            }
            $('#list-right').empty();
            $('#list-right').append(optionProSelected);
        }
    });

    function actionRuleManagement() {
        var txtRuleManagementID = $('#txtRuleManagementID').val();

        var ddlChannel = $('#ddlChannelModify option:selected').text();

        var ddlSubChannelID = $('#ddlSubChannelModify').val();
        var ddlSubChannel = $('#ddlSubChannelModify option:selected').text();

        var ddlSOBID = $('#ddlSOBModify').val();
        var ddlSOB = $('#ddlSOBModify option:selected').text();

        var ddlBusinessUnitID = $('#ddlBusinessUnitModify').val();
        var ddlBusinessUnit = $('#ddlBusinessUnitModify option:selected').text();

        var ddlFirstTier = $('#ddlFirstTier option:selected').text();
        var ddlSecondTier = $('#ddlSecondTier option:selected').text();

        var radioFirstTierChannel = $('input[name="radioFirstTierChannel"]:checked').val();
        var radioSecondTierChannel = $('input[name="radioSecondTierChannel"]:checked').val();

        var checkSendEmailToSaleSupport = $('#checkSendEmailToSaleSupport').val();
        var checkSendEmailToSaleUnderwriting = $('#checkSendEmailToSaleUnderwriting').val();

        var txtSpaceWorkingDays = $('#txtSpaceWorkingDays').val();
        var status = $('input[name="radioStatus"]:checked').val() == "true";
        var productCategory = [];
        $('#list-right li').each(function (i) {
            productCategory.push($(this).text());
        });


        if (ddlChannel == "") {
            alert('กรุณาระบุข้อมูล Channel');

        } else {


            var urlAction = apiUrl + '/MasterData/SaveRuleManagement';
            var frmdata = {
                ruleManagementID: txtRuleManagementID == "" ? null : parseInt(txtRuleManagementID),
                channel: ddlChannel,
                subChannelID: ddlSubChannelID == "" ? null : parseInt(ddlSubChannelID),
                subChannel: ddlSubChannelID == "" ? null : ddlSubChannel,
                sobID: ddlSOBID == "" ? null : ddlSOBID,
                sob: ddlSOBID == "" ? null : ddlSOB,
                businessUnitID: ddlBusinessUnitID == "" ? null : parseInt(ddlBusinessUnitID),
                businessUnit: ddlBusinessUnitID == "" ? null : ddlBusinessUnit,
                firstTier: ddlFirstTier,
                secondTier: $('#ddlSecondTier').val() == "" ? null : ddlSecondTier,
                firstTierChannel: radioFirstTierChannel,
                secondTierChannel: typeof (radioSecondTierChannel) == "undefined" ? null : radioSecondTierChannel,
                spaceWorkingDays: txtSpaceWorkingDays == "" ? null : parseInt(txtSpaceWorkingDays),
                sendEmailToSaleSupport: checkSendEmailToSaleSupport,
                sendEmailToSaleUnderwriting: checkSendEmailToSaleUnderwriting,
                status: status,
                productCategory: productCategory,
                userID: $("#userName").text()
            };

            $.ajax({
                type: "POST",
                url: urlAction,
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(frmdata),
                datatype: "json",
                success: function (data) {
                    if (data.success) {
                        alert(data.status.message)
                        dataTableRuleManagement.ajax.reload();
                        $('#actionTypeRuleManagement').val("");
                    } else if (!data.success) {
                        alert(data.status.message);
                    } else {
                        alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
                    }
                }
            });
        }
    }

    function clearRuleManagement() {
        $("#actionTypeRuleManagement").val("");
        $('#txtRuleManagementID').val("");
        $('#ddlChannelModify').val("").trigger("change");

        $('#ddlSubChannelModify').val("").trigger("change");
        $('#ddlSOBModify').val("").trigger("change");
        $('#ddlBusinessUnitModify').val("").trigger("change");
        $('#ddlFirstTier').val("Agent");
        $('#ddlSecondTier').val("");

        $('#radioStatus').prop("checked", true);
        $('#radioFirstTierChannel').prop("checked", true);
        $('input[name="radioSecondTierChannel"]').prop("checked", false);

        $('#checkSendEmailToSaleSupport').val("");
        $('#checkSendEmailToSaleUnderwriting').val("");

        $('#txtSpaceWorkingDays').val("");

        $('#list-left').empty();
        $('#list-right').empty();
    }

    $("#btnDeleteDocumentChannel").click(function () {
        if ($('#txtRuleManagementID').val() != "") {
            $("#actionTypeRuleManagement").val("delete");
            var urlAction = apiUrl + '/MasterData/DeleteRuleManagement';

            var frmdata = {
                ruleManagementID: parseInt($('#txtRuleManagementID').val()),
                userID: $("#userName").text()
            };

            $.ajax({
                type: "POST",
                url: urlAction,
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(frmdata),
                datatype: "json",
                success: function (data) {
                    if (data.success) {
                        alert(data.status.message)
                        dataTableRuleManagement.ajax.reload();
                        $('#actionTypeRuleManagement').val("");
                        $('#txtRuleManagementID').val("");
                    } else if (!data.success) {
                        alert(data.status.message);
                    } else {
                        alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
                    }
                }
            });

        } else {
            alert("กรุณาเลือกข้อมูลที่ต้องการลบ");
        }
    });

    //---- search main --//

    $('#ddlChannelMain').on('change', function (e) {
        var status = $('#ddlChannelMain option:selected').text();
        dataTableRuleManagement.column(2).search(status).draw();
        initSubChannelDdl();
    });

    $('#ddlChannelModify').on('change', function (e) {
        initSubChannelDdlMod();
    });

    $('#ddlSOBMain').on('change', function (e) {
        var status = $('#ddlSOBMain option:selected').text();
        dataTableRuleManagement.column(7).search(status).draw();
    });

    $('#ddlBusinessUnitMain').on('change', function (e) {
        var status = $('#ddlBusinessUnitMain option:selected').text();
        dataTableRuleManagement.column(10).search(status).draw();
    });


    $('#ddlProductCategoryMain').on('change', function (e) {
        var status = $('#ddlProductCategoryMain option:selected').text();
        dataTableRuleManagement.column(12).search(status).draw();
    });

    $('#ddlStatusMain').on('change', function (e) {
        var status = $(this).val();

        if (status == "true") {
            dataTableRuleManagement.column(18).search(true).draw();
        } else {
            dataTableRuleManagement.column(18).search(false).draw();
        }

    });

    $("#btnResetMain").click(function () {
        $('#ddlChannelMain').val("").trigger("change");
        $('#ddlSubChannelMain').val("").trigger("change");
        $('#ddlSOBMain').val("").trigger("change");
        $('#ddlBusinessUnitMain').val("").trigger("change");
        $('#ddlProductCategoryMain').val("").trigger("change");
        $('#ddlStatusMain').val("")
        dataTableRuleManagement.ajax.reload();
        clearRuleManagement();

    });
    //---- search main --//
});

function initChannelDdl() {
    $.ajax({
        url: apiUrl + '/MasterData/GetChannel',
        dataType: 'json',
        type: 'GET'
    }).done(function (data) {
        var dataSource = $.map(data.data, function (item) {
            return {
                text: item.description,
                id: item.channelID,
            }
        });
        $('#ddlChannelMain').select2({
            placeholder: 'Please Select',
            data: dataSource,
            sorter: function (data) {
                return data.sort(function (a, b) {
                    var n1 = parseInt(a.id);
                    var n2 = parseInt(b.id);
                    return n1 < n2 ? -1 : n1 > n2 ? 1 : 0;
                });
            }
        });
        $('#ddlChannelModify').select2({
            placeholder: 'Please Select',
            data: dataSource,
            sorter: function (data) {
                return data.sort(function (a, b) {
                    var n1 = parseInt(a.id);
                    var n2 = parseInt(b.id);
                    return n1 < n2 ? -1 : n1 > n2 ? 1 : 0;
                });
            }
        });
    });
}

function initSubChannelDdl() {
    $('#ddlSubChannelMain').empty().trigger("change");
    $.ajax({
        url: apiUrl + '/MasterData/GetSubChannel',
        dataType: 'json',
        type: 'GET'
    }).done(function (data) {
        let cId = $('#ddlChannelMain').val()
        let invalidEntries = 0
        let arr = data.data.filter(function (item) {
            if (item.channelID == cId) {
                return true
            }
            invalidEntries++
            return false;
        })
        var dataSource = $.map(arr, function (item) {
            return {
                text: item.subChannelID + " : " + item.description,
                id: item.subChannelID,
            }
        });
        $('#ddlSubChannelMain').select2({
            placeholder: 'Please Select',
            data: dataSource
        });
        $('#ddlSubChannelMain').val("").trigger("change")
    });
    $('#ddlSubChannelMain').on('change', function (e) {
        var status = $('#ddlSubChannelMain option:selected').text();
        dataTableRuleManagement.column(5).search(status).draw();
    });
}

function initSubChannelDdlMod() {
    $('#ddlSubChannelModify').empty().trigger("change");
    $.ajax({
        url: apiUrl + '/MasterData/GetSubChannel',
        dataType: 'json',
        type: 'GET'
    }).done(function (data) {
        let cId = $('#ddlChannelModify').val()
        let invalidEntries = 0
        let arr = data.data.filter(function (item) {
            if (item.channelID == cId) {
                return true
            }
            invalidEntries++
            return false;
        })
        var dataSource = $.map(arr, function (item) {
            return {
                text: item.subChannelID + " : " + item.description,
                id: item.subChannelID,
            }
        });
        $('#ddlSubChannelModify').select2({
            placeholder: 'ไม่ระบุ',
            allowClear: true,
            data: dataSource
        });
        $('#ddlSubChannelModify').val("").trigger("change")
    });
}

function bindSubChannelDdlMod(bindId) {
    $('#ddlSubChannelModify').empty().trigger("change");
    $.ajax({
        url: apiUrl + '/MasterData/GetSubChannel',
        dataType: 'json',
        type: 'GET'
    }).done(function (data) {
        let cId = $('#ddlChannelModify').val()
        let invalidEntries = 0
        let arr = data.data.filter(function (item) {
            if (item.channelID == cId) {
                return true
            }
            invalidEntries++
            return false;
        })
        var dataSource = $.map(arr, function (item) {
            return {
                text: item.subChannelID + " : " + item.description,
                id: item.subChannelID,
            }
        });
        $('#ddlSubChannelModify').select2({
            placeholder: 'ไม่ระบุ',
            allowClear: true,
            data: dataSource
        });
        $('#ddlSubChannelModify').val(bindId).trigger("change")
    });
}


function initSOBDdl() {
    $.ajax({
        url: apiUrl + '/MasterData/GetSourceOfBusiness',
        dataType: 'json',
        type: 'GET'
    }).done(function (data) {
        var dataSource = $.map(data.data, function (item) {
            return {
                text: item.sourceOfBusinesID + " : " + item.description,
                id: item.sourceOfBusinesID,
            }
        });
        $('#ddlSOBMain').select2({
            placeholder: 'Please Select',
            data: dataSource
        });
        $('#ddlSOBModify').select2({
            placeholder: 'ไม่ระบุ',
            allowClear: true,
            data: dataSource
        });
    });
}

function bindSOBDdlMod(bindId) {
    $.ajax({
        url: apiUrl + '/MasterData/GetSourceOfBusiness',
        dataType: 'json',
        type: 'GET'
    }).done(function (data) {
        var dataSource = $.map(data.data, function (item) {
            return {
                text: item.sourceOfBusinesID + " : " + item.description,
                id: item.sourceOfBusinesID,
            }
        });
        $('#ddlSOBModify').select2({
            placeholder: 'ไม่ระบุ',
            allowClear: true,
            data: dataSource
        });
        $('#ddlSOBModify').val(bindId).trigger('change');
    });
}

function initBusinessUnitDdl() {
    $.ajax({
        url: apiUrl + '/MasterData/GetBusinessUnit',
        dataType: 'json',
        type: 'GET'
    }).done(function (data) {
        var dataSource = $.map(data.data, function (item) {
            return {
                text: item.businessUnitID + " : " + item.description,
                id: item.businessUnitID,
            }
        });
        $('#ddlBusinessUnitMain').select2({
            placeholder: 'Please Select',
            data: dataSource,
            sorter: function (data) {
                return data.sort(function (a, b) {
                    var n1 = parseInt(a.id);
                    var n2 = parseInt(b.id);
                    return n1 < n2 ? -1 : n1 > n2 ? 1 : 0;
                });
            }
        });
        $('#ddlBusinessUnitModify').select2({
            placeholder: 'ไม่ระบุ',
            allowClear: true,
            data: dataSource,
            sorter: function (data) {
                return data.sort(function (a, b) {
                    var n1 = parseInt(a.id);
                    var n2 = parseInt(b.id);
                    return n1 < n2 ? -1 : n1 > n2 ? 1 : 0;
                });
            }
        });
    });
}

function bindBusinessUnitDdlMod(bindId) {
    $.ajax({
        url: apiUrl + '/MasterData/GetBusinessUnit',
        dataType: 'json',
        type: 'GET'
    }).done(function (data) {
        var dataSource = $.map(data.data, function (item) {
            return {
                text: item.businessUnitID + " : " + item.description,
                id: item.businessUnitID,
            }
        });
        $('#ddlBusinessUnitModify').select2({
            placeholder: 'ไม่ระบุ',
            allowClear: true,
            data: dataSource,
            sorter: function (data) {
                return data.sort(function (a, b) {
                    var n1 = parseInt(a.id);
                    var n2 = parseInt(b.id);
                    return n1 < n2 ? -1 : n1 > n2 ? 1 : 0;
                });
            }
        });
        $('#ddlBusinessUnitModify').val(bindId).trigger('change');
    });
}

function initProductCategoryDdl() {
    $.ajax({
        url: apiUrl + '/MasterData/GetProductCategory',
        dataType: 'json',
        type: 'GET'
    }).done(function (data) {
        var dataSource = $.map(data.data, function (item) {
            return {
                text: item.description,
                id: item.productCategoryID,
            }
        });
        $('#ddlProductCategoryMain').select2({
            placeholder: 'Please Select',
            data: dataSource
        });
    });
}