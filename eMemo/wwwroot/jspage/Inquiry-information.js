﻿var dataTableMemoMappingModal;
var dataTableAmendmentDocument;
var replyMode = false;
var uId;
var isMemoStatusRepliedOrDiscard = false;
var frmdata = {
    memoTransactionID: "",
    userID: $("#userName").text(),
    memoDocumentData: [],
    noteList: []
};
var basicInformation;

function removefile(filename, rowindex) {
    if ($('#chx_' + rowindex).is(":checked")) {
        let linkfile = $('#listFile' + rowindex + ' a:contains("' + filename + '")');
        linkfile.next().next().remove();
        linkfile.next().remove();
        linkfile.remove();
    }
}

$(document).ready(function() {
    uId = $('#memoTransactionID').val();
    frmdata.memoTransactionID = uId;
    loadDDL();
    checkRole();

    function checkRole() {
        if (localStorage.getItem('isAdministrator') == 'true') {
            $('#btnSave_EditItem').css({ pointerEvents: "none" });
            $('#btnReply').css({ pointerEvents: "none" });
            $('#btnEditMemo').css({ pointerEvents: "none" });
        }
    }

    $.ajax({
        type: "GET",
        url: apiUrl + '/Inquiry/GetMemoInquiryDetail/' + uId,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function(result) {
            noteList(result.data.noteList)
            isMemoStatusRepliedOrDiscard = (['Replied', 'Discard'].indexOf(result.data.memoDocumentInformation.ebaoMemoStatus) >= 0 ? true : false);
            loadMemoDocument(result.data.memoDocumentHistory);
            loadAmendmentDocument(result.data.memoAmendmentInformation.documents);
            loadAmendmentHistory(result.data.memoAmendmentInformation.history);
            basicInformation = result.data.basicInformation;
            loadBasicInformation(basicInformation);
            loadMemoDocumentInformation(result.data.memoDocumentInformation);
            $('#btnDeleteItemInquiry').on('click', function() {
                if ($("input:checkbox[name=checkboxSelectInquiry]:checked").length === 0) {
                    alert('กรุณาเลือกรายการที่ต้องการดำเนินการ');
                } else {
                    $("input:checkbox[name=checkboxSelectInquiry]:checked").each(function() {
                        var dataSelected = dataTableAmendmentDocument.row(this.closest('tr')).data();
                        console.log(dataSelected, 'dataSelected')
                        var memoDocItem = {
                            "memoDetailID": dataSelected["memoDetailID"],
                            "memoType": null,
                            "memoCode": dataSelected["memoCode"],
                            "comment": null,
                            "add": false,
                            "edit": false,
                            "delete": true,
                            "attachments": []
                        };
                        frmdata.memoDocumentData.push(memoDocItem);

                        $(this).closest('tr').hide();
                        // $(this).prop("checked", false);
                    });
                }
            });
            if (isMemoStatusRepliedOrDiscard) {
                $('#divEditMemo').hide();
                $('#btnSave_EditItem').prop("disabled", true);
                $("#btnSave_EditItem").css({ cursor: "not-allowed" });
            }
        }
    });
    searchModalMemoData();
    // $('#selectcode').selectize({
    //     sortField: 'text'
    // });
    $('#ddlMemoType').on('change', function() {
        filterMemoData();
    });
    $('#ddlMemoCode').on('change', function() {
        filterMemoData();
    });

    $('#modalAddItem').on('hidden.bs.modal', function(e) {
        resetMemoModal();
    });

    $("#btnModalReset").click(function() {
        resetMemoModal();
    });
    $('#btnModalSearch').click(function() {
        searchModalMemoData();
    });
    $('#btnModalAdd').click(function() {
        var newlists = [];
        $("input:checkbox[name=checkboxModalSelect]:checked").each(function() {
            var dataSelected = dataTableMemoMappingModal.row(this.closest('tr')).data();
            var filteredData = dataTableAmendmentDocument
                .column(2)
                .data()
                .filter(function(value, index) {
                    return value == dataSelected["memoCode"] ? true : false;
                });

            //ไม่มี memoCode
            if (filteredData.length === 0) {
                // var memoDocItem = {
                //     "memoType": dataSelected["memoTypeName"],
                //     "memoCode": dataSelected["memoCode"],
                //     "comment": null,
                //     "add": true,
                //     "edit": false,
                //     "delete": false,
                //     "attachments": []
                // };
                // frmdata.memoDocumentData.push(memoDocItem);

                let newitem = {
                    "data": {
                        "memoDetailID": null,
                        "sequence": null,
                        "memoCode": dataSelected["memoCode"],
                        "memoDescription": dataSelected["description"],
                        "comment": null,
                        "letterStatus": "",
                        "replyDate": null,
                        "replyBy": null,
                        "source": null
                    },
                    "memoType": dataSelected["memoTypeName"]
                }
                newlists.push(newitem);
            } else {
                filteredData = Array.prototype.slice.call(filteredData);
                filteredData = [...new Set(filteredData)];
                // console.log(filteredData)

                $.each(filteredData, function(i, v) {

                    var indexes = dataTableAmendmentDocument.rows().eq(0).filter(function(rowIdx) {
                        return dataTableAmendmentDocument.cell(rowIdx, 2).data() === v ? true : false;
                    });
                    if (indexes.length > 0) {
                        let check = false;
                        $.each(indexes, function(i, v) {
                            console.log(dataTableAmendmentDocument.cell(v, 9).data());
                            if (dataTableAmendmentDocument.cell(v, 9).data() === null || dataTableAmendmentDocument.cell(v, 9).data().match(/Replied|Discard/i) === null) {
                                check = true;
                            }
                        });
                        if (check == false) {
                            let newitem = {
                                "data": {
                                    "memoDetailID": null,
                                    "sequence": null,
                                    "memoCode": dataSelected["memoCode"],
                                    "memoDescription": dataSelected["description"],
                                    "comment": null,
                                    "letterStatus": "",
                                    "replyDate": null,
                                    "replyBy": null,
                                    "source": null
                                },
                                "memoType": dataSelected["memoTypeName"]
                            }
                            newlists.push(newitem);
                        }

                    }


                });

            }

        });
        $.each(newlists, function(index, item) {
            dataTableAmendmentDocument.row.add(item.data).draw(false);
            var i = dataTableAmendmentDocument.rows().count() - 1;
            $('#chx_' + i).prop("disabled", false);
            $('#chx_' + i).attr("itemadd", true);
            $('#chx_' + i).attr('memotype', item.memoType);
        });

        alert('เพิ่ม Memo Code เรียบร้อยแล้ว');
    });
    $('#btnPreview').on('click', function() {
        window.open(apiUrl + '/FileAttachment/PreviewMemoAttachment/' + $("#ebaoDocumentNo").text(), '_blank');
    });
    resetDisplay();

    $("#modalResend").on('show.bs.modal', function(event) {
        var callModal = $(event.relatedTarget);
        var id = callModal.data('id');
        // console.log(id);
        $('#ddlModalResendVia').val('').trigger('change')
        $('#ddlModalResendByChannel').val('').trigger('change')
        $('#txtModalResendInformation').val('')
        var modal = $(this);
        modal.find('#hModalResendDocHistoryId').val(id);
    });

    $('#ddlModalResendVia').on('change', function() {
        var sendTo = $('#ddlModalResendVia').val();
        var sendBy = $('#ddlModalResendByChannel').val();
        setResend(sendTo, sendBy);
    });

    $('#ddlModalResendByChannel').on('change', function() {
        var sendTo = $('#ddlModalResendVia').val();
        var sendBy = $('#ddlModalResendByChannel').val();
        setResend(sendTo, sendBy);
    });
});
window.onbeforeunload = function(e) {
    return 'Dialog text here.';
};

function setResend(sendTo, sendBy) {
    if (sendBy !== '') {
        if (sendTo == 'Agent') {
            $('#txtModalResendInformation').val((sendBy == 'SMS') ? basicInformation.agentPhoneNumber : basicInformation.agentEmail);
        } else if (sendTo == 'Customer') {
            $('#txtModalResendInformation').val((sendBy == 'SMS') ? basicInformation.customerPhoneNumber : basicInformation.customerEmail);
        } else if (sendTo == 'Service Agent') {
            $('#txtModalResendInformation').val((sendBy == 'SMS') ? basicInformation.serviceAgentPhoneNumber : basicInformation.serviceAgentEmail);
        } else {
            $('#txtModalResendInformation').val('');
        }
    }
}

function searchModalMemoData() {
    $.ajax({
        type: "GET",
        url: apiUrl + '/MasterData/GetMemoMapping',
        contentType: "application/json; charset=utf-8",
        success: function(result) {
            loadMemoList(result.data);
        }
    });
}

function filterMemoData() {
    var mType = $('#ddlMemoType').val();
    if (mType != "") {
        dataTableMemoMappingModal.column(1).search(mType).draw();
    }
    var mCode = $('#ddlMemoCode').val();
    if (mCode != "") {
        dataTableMemoMappingModal.column(3).search(mCode).draw();
    }
}

function reloadMemoDocument() {
    $.ajax({
        type: "GET",
        url: apiUrl + '/Inquiry/GetMemoInquiryDetail/' + uId,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function(result) {
            loadAmendmentDocument(result.data.memoAmendmentInformation.documents);
            $('#tableMemoDocument').DataTable().clear().destroy();
            loadMemoDocument(result.data.memoDocumentHistory);
        }
    });
}

function resetMemoModal() {
    $('#ddlMemoType').val("");
    $('#ddlMemoCode').val("").trigger("change");
    searchModalMemoData();
}

$('#btnEditMemo').click(function() {
    $('#divEditMemo').hide();
    $('.divEditItem').show();
    $("#tableAmendmentDocument").css("pointer-events", "");
    $('#inquiry-select-all').prop('disabled', false);
    $("input:checkbox[name=checkboxSelectInquiry]").each(function() {
        $(this).prop("disabled", false);
    });
});

$('#btnReply').click(function() {
    replyMode = true;
    $('#divEditMemo').hide();
    $("#tableAmendmentDocument").css("pointer-events", "");
    $('#inquiry-select-all').prop('disabled', false);
    $("input:checkbox[name=checkboxSelectInquiry]").each(function() {
        $(this).prop("disabled", false);
    });
    $("input:checkbox[name=checkboxSelectInquiry]").each(function() {
        $(this).prop("disabled", false);
        //$('#reply_' + $(this).val()).removeAttr("disabled");
        //$('#letter_' + $(this).val()).removeAttr("disabled");
    });
});

$('#btnSave_EditItem').click(function() {
    if (replyMode) {
        replyMemoItem();
    } else {
        replyMode = false;
        saveMemoItems();
    }
});

function replyMemoItem() {
    var frmReplyMemoItem = {
        "memoTransactionID": uId,
        "userID": $("#userName").text(),
        "memoReplyList": []
    };
    if ($("input:checkbox[name=checkboxSelectInquiry]:checked").length === 0) {
        alert('กรุณาเลือกรายการที่ต้องการดำเนินการ');
    } else {
        $("input:checkbox[name=checkboxSelectInquiry]:checked").each(function() {
            var rowIndex = $(this).val();
            var dataSelected = dataTableAmendmentDocument.row(this.closest('tr')).data();
            if ($('#reply_' + rowIndex).val() == "") {
                alert('กรุณาระบุวันที่บันทึกรับรายการ');
            } else {
                var memoDocItem = {
                    "memoDetailID": dataSelected["memoDetailID"],
                    "memoCode": dataSelected["memoCode"],
                    "replyDate": convertThaiToUS($('#reply_' + rowIndex).val()).toJSON(),
                    "letterStatus": $('#letter_' + rowIndex).val()
                };
                frmReplyMemoItem.memoReplyList.push(memoDocItem);
            }
        });
        // console.log(frmReplyMemoItem)
        var btnHtmlBefore = $('#btnSave_EditItem').html();
        buttonLoading('btnSave_EditItem', true);
        $.ajax({
            type: "POST",
            url: apiUrl + '/Inquiry/ReplyMemoItem',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(frmReplyMemoItem),
            datatype: "json",
            success: function(data) {
                buttonLoading('btnSave_EditItem', false, btnHtmlBefore);
                if (data.success) {
                    reloadMemoDocument();
                    resetDisplay();
                    alert(data.status.message);
                } else if (!data.success) {
                    alert(data.status.message);
                } else {
                    alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
                }
            },
            error: function() {
                alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
                buttonLoading('btnSave_EditItem', false, btnHtmlBefore);
            }
        });

    }
}

$('#btnModalReplyOK').click(function() {
    var radioSelected = $('input[name=radioSend]:checked').val();
    if (radioSelected === 'send') {
        var frmSendMemo = {
            "memoTransactionID": uId,
            "sendTo": $('#ddlSendNowTo').val(),
            "userID": $("#userName").text()
        };
        //   console.log(frmSendMemo)
        $.ajax({
            type: "POST",
            url: apiUrl + '/Inquiry/SendMemo',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(frmSendMemo),
            datatype: "json",
            success: function(data) {
                if (data.success) {
                    $('#modalReply').modal('hide');
                    alert(data.status.message);
                } else if (!data.success) {
                    alert(data.status.message);
                } else {
                    alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
                }
            }
        });
    } else {
        $('#modalReply').modal('hide');
    }
});

function saveMemoItems() {
    console.log('Save');
    var btnHtmlBefore = $('#btnSave_EditItem').html();
    buttonLoading('btnSave_EditItem', true);
    if ($("input:checkbox[name=checkboxSelectInquiry]:checked").length === 0 &&
        frmdata.memoDocumentData.length === 0 &&
        getNoteItems().length === 0 &&
        $("input:checkbox[name=checkboxSelectInquiry][itemadd]").length === 0) {
        alert('กรุณาเลือกรายการที่ต้องการดำเนินการ');
        buttonLoading('btnSave_EditItem', false, btnHtmlBefore);
    } else if ($("input:checkbox[name=checkboxSelectInquiry]:checked").length !== 0 &&
        frmdata.memoDocumentData.length !== 0 &&
        $("input:checkbox[name=checkboxSelectInquiry][itemadd]").length === 0) {
        //case delete only
        console.log('case delete only')
        callSaveMemoDoc(frmdata, btnHtmlBefore);
    } else if ($("input:checkbox[name=checkboxSelectInquiry]:checked").length === 0 &&
        frmdata.memoDocumentData.length === 0 &&
        getNoteItems().length !== 0 &&
        $("input:checkbox[name=checkboxSelectInquiry][itemadd]").length === 0) {
        //case note list only
        console.log('case note only')
        callSaveMemoDoc(frmdata, btnHtmlBefore);
    } else {
        console.log('case all');
        $("input:checkbox[name=checkboxSelectInquiry]:checked").each(function() {
            $(this).removeAttr('itemadd');
            // console.log(this);
        });
        var rowTotal = $("input:checkbox[name=checkboxSelectInquiry]:checked").length;
        rowTotal += $("input:checkbox[name=checkboxSelectInquiry][itemadd]").length;
        // console.log(rowTotal);
        var rowCount = 1;
        dataTableAmendmentDocument.rows().every(function(rowIdx, tableLoop, rowLoop) {
            var data = this.data();
            var chx_attr = $('#chx_' + rowIdx).attr('memotype');
            var chx_attr_exist = (typeof chx_attr !== 'undefined' && chx_attr !== false);
            if ($('#chx_' + rowIdx).is(":checked") || $('#chx_' + rowIdx).is("[itemadd]")) {
                var memoDocItem = {
                    "memoType": (chx_attr_exist) ? chx_attr : null,
                    "memoCode": data.memoCode,
                    "memoDetailID": data.memoDetailID,
                    "comment": $('#comment_' + rowIdx).val(),
                    "add": (chx_attr_exist) ? true : false,
                    "edit": (chx_attr_exist) ? false : true,
                    "delete": false
                };
                let elementfiles = $('#listFile' + rowIdx + ' a');
                let listFiles = new DataTransfer();

                $.each(elementfiles, function(i, v) {
                    (async() => {
                        createFile($(v).text(), v.href).then(function(resultFile) {
                            listFiles.items.add(resultFile);
                        });
                    })()
                });


                setTimeout(function() {
                    let file = $('#file_' + rowIdx)[0].files;
                    if (file.length != 0) {
                        listFiles.items.add(file[0]);
                    }
                    if (listFiles.files.length != 0) {
                        let filesArray = Array.prototype.slice.call(listFiles.files)
                        Promise.all(filesArray.map(getBase64andfile)).then((values) => {
                            let attachment = [];
                            $.each(values, function(i, v) {
                                attachment.push(v);
                            });
                            memoDocItem["attachments"] = attachment;

                            let c = rowCount++;
                            frmdata.memoDocumentData.push(memoDocItem);
                            if (rowTotal === c) {
                                callSaveMemoDoc(frmdata, btnHtmlBefore);
                            }
                        });
                    } else {
                        memoDocItem["attachments"] = null;
                        let c = rowCount++;
                        frmdata.memoDocumentData.push(memoDocItem);
                        if (rowTotal === c) {
                            callSaveMemoDoc(frmdata, btnHtmlBefore);
                        }
                    }
                }, 3000);


                // var file = $('#file_' + rowIdx)[0].files;
                // getBase64(file[0]).then(function(base64string) {
                //     var attachment = [{
                //         "fileName": file[0].name,
                //         "fileBase64": base64string,
                //         "fileSize": file[0].size,
                //         "contentType": file[0].type
                //     }];
                //     memoDocItem["attachments"] = attachment;
                // }).catch(e => {
                //     memoDocItem["attachments"] = null;
                // }).finally(() => {
                //     var c = rowCount++;
                //     // console.log(c);                   

                //     frmdata.memoDocumentData.push(memoDocItem);
                //     if (rowTotal === c) {
                //         callSaveMemoDoc(frmdata);
                //     }
                // });
            }
        });
    }
}

function callSaveMemoDoc(frmdata, btnHtmlBefore) {
    // console.log(frmdata);
    frmdata.userID = $("#userName").text();
    frmdata.noteList = getNoteItems();

    $.ajax({
        type: "POST",
        url: apiUrl + '/Transaction/SaveMemoDocument',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(frmdata),
        datatype: "json",
        success: function(data) {
            buttonLoading('btnSave_EditItem', false, btnHtmlBefore);
            if (data.success) {
                reloadMemoDocument();
                resetDisplay();
                $('#modalReply').modal('show');
                // alert(data.status.message);
            } else if (!data.success) {
                alert(data.status.message);
            } else {
                alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
            }
            frmdata.memoDocumentData = [];
        },
        error: function() {
            alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
            buttonLoading('btnSave_EditItem', false, btnHtmlBefore);
            frmdata.memoDocumentData = [];
        }
    });
}
$('#inquiry-select-all').click(function() {
    var checked = $('#inquiry-select-all').prop('checked');
    $("input:checkbox[name=checkboxSelectInquiry]").each(function() {
        $(this).prop("checked", checked);
        if (replyMode) {
            $('#reply_' + $(this).val()).prop("disabled", !checked);
            $('#letter_' + $(this).val()).prop("disabled", !checked);
        }
    });
});

$('#btnEditItemInquiry').click(function() {
    if ($("input:checkbox[name=checkboxSelectInquiry]:checked").length === 0) {
        alert('กรุณาเลือกรายการที่ต้องการดำเนินการ');
    } else {
        $("input:checkbox[name=checkboxSelectInquiry]:checked").each(function() {
            $('.removefile_' + $(this).val()).show();
            var rowdata = dataTableAmendmentDocument.row($(this).val()).data();
            // console.log(rowdata);
            $('#comment_' + $(this).val()).removeAttr("disabled");
            if ($('#comment_' + $(this).val()).val() !== null) {
                $('#comment_' + $(this).val()).val(rowdata.memoDescription);
            }
            $('#file_' + $(this).val()).removeAttr("disabled");
            $('#divFile' + $(this).val()).css("display", "block");
        });
    }
});




$(document).on("change", "input:checkbox[name=checkboxSelectInquiry]", function() {
    if (this.checked) {
        if (replyMode) {
            var checked = $(this).prop('checked');
            $('#reply_' + $(this).val()).prop("disabled", !checked);
            $('#reply_' + $(this).val()).val(moment().add(543, 'year').format('DD/MM/YYYY'));
            $('#letter_' + $(this).val()).prop("disabled", !checked);
        }
    }
});

// $("input:checkbox[name=checkboxSelectInquiry]").click(function() {
//     if (replyMode) {
//         var checked = $(this).prop('checked');
//         $('#reply_' + $(this).val()).prop("disabled", !checked);
//         $('#letter_' + $(this).val()).prop("disabled", !checked);
//     }
// });

function resetDisplay() {
    $('#divEditMemo').show();
    $('.divEditItem').hide();
    $("#tableAmendmentDocument").css("pointer-events", "none");
    $('#inquiry-select-all').prop('checked', false);
    $('#inquiry-select-all').prop('disabled', true);
    $("input:checkbox[name=checkboxSelectInquiry]").each(function() {
        $(this).prop("checked", false);
        $(this).prop("disabled", true);
        $('#comment_' + $(this).val()).attr("disabled", true);
        $('#file_' + $(this).val()).attr("disabled", true);
        $('#divFile' + $(this).val()).css("display", "none");
        $('#reply_' + $(this).val()).attr("disabled", true);
        $('#letter_' + $(this).val()).attr("disabled", true);
    });
}

$('#selectAll_add').click(function() {
    var checked = $('#selectAll_add').prop('checked');
    $("input:checkbox[name=checkboxSelectAdd]").each(function() {
        $(this).prop("checked", checked);
    });
});

$("#modalAddItem").on('hide.bs.modal', function() {
    $('#selectAll_add').prop("checked", false);
    $("input:checkbox[name=checkboxSelectAdd]").each(function() {
        $(this).prop("checked", false);
    });
});

function loadMemoDocument(viewModel) {
    var jsonDataMemoDocument = [{
            "memoDocumentHistoryID": 8,
            "documentNo": "2011110000005-1",
            "transactionDate": "24/12/2021",
            "osMemo": 0,
            "printedBy": "SUPERUSER20"
        },
        {
            "memoDocumentHistoryID": 24,
            "documentNo": "2011110000005-10",
            "transactionDate": "04/01/2022",
            "osMemo": 5,
            "printedBy": "testusers"
        },
        {
            "memoDocumentHistoryID": 14,
            "documentNo": "2011110000005-2",
            "transactionDate": "04/01/2022",
            "osMemo": 1,
            "printedBy": "testusers"
        },
        {
            "memoDocumentHistoryID": 16,
            "documentNo": "2011110000005-3",
            "transactionDate": "04/01/2022",
            "osMemo": 2,
            "printedBy": "testusers"
        },
        {
            "memoDocumentHistoryID": 19,
            "documentNo": "2011110000005-4",
            "transactionDate": "04/01/2022",
            "osMemo": 3,
            "printedBy": "testusers"
        },
        {
            "memoDocumentHistoryID": 23,
            "documentNo": "2011110000005-5",
            "transactionDate": "04/01/2022",
            "osMemo": 4,
            "printedBy": "testusers"
        }
    ];

    $('#tableMemoDocument').DataTable({
        responsive: true,
        autoWidth: false,
        searching: false,
        paging: false,
        info: false,
        data: viewModel,
        columns: [{
                data: 'memoDocumentHistoryID',
                visible: false,
                searchable: false
            },
            {
                data: 'documentNo',
                title: 'eBao Document No.',
                className: 'text-center',
                orderable: false,
                render: function(data, type, row) {
                    return '<a href="#home">' + data + '</a>';
                }
            },
            {
                data: 'transactionDate',
                title: 'eMemo Transaction Date',
                className: 'text-center',
                orderable: false,
                render: function(data, type, row) {
                    return moment(data, 'DD/MM/YYYY').add(543, 'year').format('DD/MM/YYYY');
                }
            },
            {
                data: 'osMemo',
                title: 'O/S Memo',
                className: 'text-center',
                orderable: false,
            },
            {
                data: 'printedBy',
                title: 'Printed By',
                className: 'text-center',
                orderable: false,
            },
            {
                data: 'View',
                title: 'View',
                className: 'text-center',
                orderable: false,
                // render: function (data, type, row) {
                //     return '<a href="/eMemo/Inquiry/Information"><ion-icon style="color:red;font-size:25px;cursor:pointer" name="document-attach"></ion-icon></a>';
                // }
                render: function(data, type, row) {
                    return '<a href="' + apiUrl + '/FileAttachment/PreviewMemoAttachmentHistory/' + row['documentNo'] + '" target="_blank"><ion-icon style="color:red;font-size:25px;cursor:pointer" name="document-attach"></ion-icon></a>';
                }
            },
            {
                title: 'Resend',
                className: 'text-center',
                orderable: false,
                render: function(data, type, row) {
                    if (localStorage.getItem('isAdministrator') == 'true') {
                        return '<ion-icon style="color:blue;font-size:25px;cursor:pointer" name="mail-outline"  class="linkResend"></ion-icon>';
                    } else {
                        return '<ion-icon style="color:blue;font-size:25px;cursor:pointer" name="mail-outline"  data-toggle="modal" data-target="#modalResend" data-id="' + row["memoDocumentHistoryID"] + '" ></ion-icon>';

                    }
                }
            },
        ],
        order: [
            [0, 'asc']
        ],
    });
}

function viewFile(transactionID) {
    console.log(transactionID);
}

function loadAmendmentDocument(viewModel) {
    // var jsonDataAmendmentDocument = [
    //     { "SEQ": "1", "MemoCode": "HOLDER", "MemoDescription": "ตรวจสุขภาพผู้ชำระเบี้ยโดยแพทย์ ตามแบบปอร์มของบริษัท", "Comment": "", "ReplyDate": "", "LetterStatus": "", "ReplyBy": "", "ReplySource": "" },
    //     { "SEQ": "2", "MemoCode": "HX", "MemoDescription": "ตรวจสุขภาพโดยแพทย์ ตามแบบปอร์มของบริษัท เนื่องจากประวัติสุขภาพ", "Comment": "", "ReplyDate": "", "LetterStatus": "", "ReplyBy": "", "ReplySource": "" }
    // ];
    if (dataTableAmendmentDocument !== null) {
        $('#tableAmendmentDocument').DataTable().destroy();
        dataTableAmendmentDocument = null;
        $('#tableAmendmentDocument').empty();
    }
    $.fn.dataTable.ext.errMode = 'none';
    dataTableAmendmentDocument = $('#tableAmendmentDocument').DataTable({
        "scrollX": true,
        "sScrollXInner": "100%",
        paging: false,
        bInfo: false,
        data: viewModel,
        columns: [{
                targets: 0,
                searchable: false,
                orderable: false,
                className: 'dt-body-center',
                render: function(data, type, row, meta) {
                    if (type == 'display') {
                        // if (!!row["letterStatus"] && row["letterStatus"].match(/Replied|Discard/i) !== null) {
                        //     return '<input disabled type="checkbox" id="chx_' + meta.row + '"  name="checkboxSelectInquiry_replied" value="' + meta.row + '" style="opacity:0;">';
                        // } else {
                        //     return '<input disabled type="checkbox" id="chx_' + meta.row + '"  name="checkboxSelectInquiry" value="' + meta.row + '">';
                        // }
                        return '<input disabled type="checkbox" id="chx_' + meta.row + '"  name="checkboxSelectInquiry" value="' + meta.row + '">';
                    }
                }
            },
            { data: 'sequence', title: 'SEQ' },
            { data: 'memoCode', title: 'Memo Code' },
            { data: 'memoDescription', title: 'Memo Description' },
            {
                data: 'comment',
                title: 'Comment',
                render: function(data, type, row, meta) {
                    return '<input disabled id="comment_' + meta.row + '" class="form-control text-input" value="' + ((data === null) ? "" : data) + '"/>';
                }
            },
            {
                data: 'fileAttachments',
                title: '',
                searchable: false,
                orderable: false,
                render: function(data, type, row, meta) {
                    if (data === undefined) {
                        return '<div><label id="divFile' + meta.row + '" style="display:none" class="file"><input disabled type="file" id="file_' + meta.row + '" aria-label="File browser example" accept=".pdf"><span class="file-custom"></span></label></div>'
                    } else {
                        var html = '<div id="listFile' + meta.row + '">';
                        if (data.length > 0) {
                            $.each(data, function(i, val) {
                                html += '<a href="' + val.filePath + '" target="_blank" style="pointer-events: auto;">' + val.fileName + "</a><span onclick='removefile(\"" + val.fileName + "\",\"" + meta.row + "\")'><i class='fas fa-trash removefile_" + meta.row + " ml-2' style='color:red;cursor:pointer'></i></span><br/>";

                            });
                            $('.removefile_' + meta.row).hide();
                        }
                        html += '<label id="divFile' + meta.row + '" style="display:none" class="file"><input disabled type="file" id="file_' + meta.row + '" aria-label="File browser example" accept=".pdf"><span class="file-custom"></span></label></div>'
                        return html
                    }
                }
            },
            { title: "Created Date", data: "createdDate" },
            { title: "Modify Date", data: "modifyDate" },
            {
                data: 'replyDate',
                title: 'Reply Date',
                render: function(data, type, row, meta) {
                    if (!!data) {
                        var dateFormat = moment(data).add(543, 'year').format('DD/MM/YYYY');
                        return '<div class="input-group"><input disabled id="reply_' + meta.row + '"   class="form-control text-input datepicker" type="text" placeholder="dd/mm/yyyy" value="' + dateFormat + '" autocomplete="off" /><div class="input-group-append"><span class="input-group-text"><i class="fa fa-calendar-alt fa-fw"></i></span></div></div>';
                    } else {
                        return '<div class="input-group"><input disabled id="reply_' + meta.row + '"   class="form-control text-input datepicker" type="text" placeholder="dd/mm/yyyy" autocomplete="off" /><div class="input-group-append"><span class="input-group-text"><i class="fa fa-calendar-alt fa-fw"></i></span></div></div>';
                    }

                }
            },
            {
                data: 'letterStatus',
                title: 'Letter Status',
                render: function(data, type, row, meta) {
                    //value="' + ((data !== "") ? data : "") + '"
                    var $select = $('<select disabled id="letter_' + meta.row + '"  class="custom-select custom-select-sm"><option value="" selected>Please Select</option ><option value="Discard">Discard</option><option value="Replied">Replied</option></select >');
                    if (data !== "") {
                        $select.find('option[value=""]').removeAttr("selected");
                        $select.find('option[value="' + data + '"]').attr('selected', 'selected');
                    }
                    return $select[0].outerHTML;
                }
            },
            { data: 'replyBy', title: 'Reply By' },
            {
                data: 'source',
                title: 'Source',
            },
            {
                data: 'memoDetailID',
                visible: false,
            }
        ],
        columnDefs: [
            { "width": "18%", "targets": 3 },
            { "width": "18%", "targets": 4 }
        ],
        drawCallback: function(settings) {
            initDatepicker();
        }
    });

}

function loadAmendmentHistory(viewModel) {
    var jsonDataHistory = [
        { "Sendto": "Agent", "Channel": "SMS", "Description": "066-5155555", "SendDate": "16/06/2563", "SentTimes": "16:40", "SendBy": "System", "SendStatus": "Successfully" }, { "Sendto": "Customer", "Channel": "E-mail", "Description": "Customer@gmail.com", "SendDate": "23/06/2563", "SentTimes": "16:40", "SendBy": "System", "SendStatus": "Successfully" }
    ];

    $('#tableHistory').dataTable({
        data: viewModel,
        columns: [
            { data: 'sendTo', title: 'Send to' },
            { data: 'channel', title: 'Channel', className: 'text-center' },
            { data: 'description', title: 'Description' },
            { data: 'sendDate', title: 'Send Date', className: 'text-center' },
            { data: 'sendTime', title: 'Sent Times', className: 'text-center' },
            { data: 'sendBy', title: 'Send By', className: 'text-center' },
            { data: 'sendStatus', title: 'Send Status', className: 'text-center' },
        ]


    });
}

function loadMemoList(memoData) {
    // var jsonDataMemoList = [
    //     { "MemoType": "MED", "MemoCode": "HOLDER", "Detail": "ตรวจสุขภาพผู้ชำระเบี้ยโดยแพทย์ ตามแบบปอร์มของบริษัท" },
    //     { "MemoType": "MED", "MemoCode": "HX", "Detail": "ตรวจสุขภาพโดยแพทย์ ตามแบบปอร์มของบริษัท เนื่องจากประวัติสุขภาพ" }
    // ];
    if (dataTableMemoMappingModal !== null) {
        $('#tableMemoList').DataTable().destroy();
        dataTableMemoMappingModal = null;
        $('#tableMemoList').empty();
    }
    dataTableMemoMappingModal = $('#tableMemoList').DataTable({
        responsive: true,
        autoWidth: false,
        // searching: false,
        paging: false,
        info: false,
        data: memoData,
        columns: [{
                targets: 0,
                searchable: false,
                orderable: false,
                className: 'dt-body-center',
                render: function(data, type, row) {
                    return '<input type="checkbox"  name="checkboxModalSelect" value="' + row["memoCodeID"] + '">';
                }
            },
            { data: "memoTypeID", visible: false },
            { data: 'memoTypeName', title: 'Memo Type' },
            { data: "memoCodeID", visible: false },
            { data: 'memoCode', title: 'Memo Code' },
            { data: 'description', title: 'Detail' }
        ]
    });
    $('#tableMemoList_filter').addClass('d-none');
    filterMemoData();
}

function loadBasicInformation(viewModel) {
    $('#policyNo').text(viewModel.policyNo);
    $('#proposalNo').text(viewModel.proposalNo);
    $('#proposalDate').text(viewModel.proposalDate);
    $('#policyStatus').text(viewModel.policyStatus);
    $('#policyName').text(viewModel.policyName);
    $('#citizenID').text(viewModel.citizenID);
    $('#mainPlan').text(viewModel.mainPlan);
    $('#agentName').text(viewModel.agentName);
    $('#unit').text(viewModel.unit);
    $('#group').text(viewModel.group);
}

function loadMemoDocumentInformation(viewModel) {
    $('#ebaoDocumentNo').text(viewModel.ebaoDocumentNo);
    $('#ebaoDocumentNo2').text(viewModel.ebaoDocumentNo);
    $('#transactionDate').text(moment(viewModel.transactionDate, 'DD/MM/YYYY').add(543, 'year').format('DD/MM/YYYY'));
    $('#createdUser').text(viewModel.createdUser);
    $('#ebaoMemoStatus').text(viewModel.ebaoMemoStatus);
    if (!!viewModel.ebaoRelpliedDate && viewModel.ebaoRelpliedDate !== '-') {
        $('#ebaoRelpliedDate').text(moment(viewModel.ebaoRelpliedDate, 'DD/MM/YYYY').add(543, 'year').format('DD/MM/YYYY'));
    } else {
        $('#ebaoRelpliedDate').text(viewModel.ebaoRelpliedDate);
    }
    $('#ebaoRelpliedUser').text(viewModel.ebaoRelpliedUser);
}

// function loadDDL() {
//     $(".ddlMemoType").empty();
//     $(".ddlMemoType").append("<option value=''>Please select</option>");
//     $.ajax({
//         url: apiUrl + '/MasterData/GetMemoType',
//         //url: 'https://localhost:44312/api/preparation/',
//         success: function (data) {
//             $.each(data.data, function (i, val) {
//                 $(".ddlMemoType").append("<option value=" + val.memoTypeID + ">" + val.memoTypeName + "</option>");
//             });
//         }
//     });

//     $("#ddlMemoCode").empty();
//     $("#ddlMemoCode").append("<option value=''>Please select</option>");
//     $.ajax({
//         url: apiUrl + '/MasterData/GetMemoCode',
//         success: function (data) {
//             $.each(data.data, function (i, val) {
//                 $("#ddlMemoCode").append("<option value='" + val.code + "'>" + val.message + "</option>");
//             });
//         }
//     });

// }

function loadDDL() {
    $("#ddlMemoType").empty();
    $("#ddlMemoType").append("<option value=''>Please select</option>");
    $.ajax({
        url: apiUrl + '/MasterData/GetMemoType',
        success: function(data) {
            $.each(data.data, function(i, val) {
                $("#ddlMemoType").append("<option value=" + val.memoTypeID + ">" + val.memoTypeName + "</option>");
            });
        }
    });


    $('#ddlMemoCode').select2({
        placeholder: 'Please Select',
        cache: true,
        ajax: {
            url: apiUrl + '/MasterData/GetMemoCodeBy',
            dataType: 'json',
            type: 'GET',
            data: function(params) {
                return {
                    param: params.term,
                    type: $("#ddlMemoType").val()
                };
            },
            processResults({ data }) {
                return {
                    results: $.map(data, function(item) {
                        return {
                            text: item.memoCode + ' - ' + item.description,
                            id: item.memoCodeID,
                        }
                    })
                }
            }
        }
    });
}

$("#btnModalResendOK").click(function() {
    var via = $("#ddlModalResendVia").val();
    var channel = $("#ddlModalResendByChannel").val();
    var information = $("#txtModalResendInformation").val();
    if (via != "") {
        if (channel != "") {
            if (information != "") {
                var validateInformation = false;
                if (channel == 'E-Mail') {
                    validateInformation = (!!validateEmail(information)) ? true : false;
                } else {
                    validateInformation = (!!validatePhoneNumber(information)) ? true : false;
                }

                if (validateInformation == false && channel == 'E-Mail') {
                    alert('E-Mail ไม่ถูกต้อง')
                } else if (validateInformation == false && channel == 'SMS') {
                    alert('เบอร์โทรศัพท์ไม่ถูกต้อง')
                } else {
                    var btnHtmlBefore = $('#btnModalResendOK').html();
                    buttonLoading('btnModalResendOK', true);

                    var frmdataResend = {
                        memoDocumentHistoryID: $('#hModalResendDocHistoryId').val(),
                        via: via,
                        channel: channel,
                        information: information,
                        userID: $("#userName").text()
                    };
                    $.ajax({
                        type: "POST",
                        url: apiUrl + '/Inquiry/ResendMemo',
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(frmdataResend),
                        datatype: "json",
                        success: function(result) {
                            alert(result.status.message);
                            if (result.status.code == "200") {
                                $("#modalResend").modal('hide');
                            }

                            buttonLoading('btnModalResendOK', false, btnHtmlBefore);
                        },
                        error: function() {
                            alert('ระบบเกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ');
                            buttonLoading('btnModalResendOK', false, btnHtmlBefore);
                        }
                    });
                }

            } else if (channel == 'E-Mail') {
                alert("กรุณาระบุ E-Mail");
            } else {
                alert("กรุณาระบุเบอร์โทรศัพท์");
            }

        } else {
            alert("กรุณาระบุช่องทางการ Resend");
        }
    } else {
        alert("กรุณาระบุผู้ที่ต้องการส่ง");
    }
});

function noteList(dataSrc) {
    if (dataSrc === undefined) {
        dataSrc = []
    }
    var html = ''
    $.each(dataSrc, function(index, value) {
        var divNoteItem = 'divNoteItem_' + index
        html += '<div id="' + divNoteItem + '" class="row mb-2">'
        html += '<div class="col-10">'
        html += '<input name="notelistitem" type="text" value="' + value + '" class="form-control text-input" />'
        html += '</div>'
        html += '<div class="col"><button class="btn btn-sm" onclick="removeNoteItem(' + divNoteItem + ')">'
        html += '<i class="fa fa-trash" aria-hidden="true"></i></button></div>'
        html += '</div>'
    })
    $('#divNoteList').html(html)

    $('#btnAddNote').on('click', function() {
        var inputTypes = [];
        $('input[name="notelistitem"]').each(function() {
            inputTypes.push($(this).prop('type'));
        });
        var divNoteItem = 'divAddNoteItem_' + inputTypes.length
        var htmlAdd = '<div id="' + divNoteItem + '" class="row mb-2">'
        htmlAdd += '<div class="col-10">'
        htmlAdd += '<input name="notelistitem" type="text" value="" class="form-control text-input" />'
        htmlAdd += '</div>'
        htmlAdd += '<div class="col"><button class="btn btn-sm" onclick="removeNoteItem(' + divNoteItem + ')">'
        htmlAdd += '<i class="fa fa-trash" aria-hidden="true"></i></button></div>'
        htmlAdd += '</div>'
        $('#divNoteList').append(htmlAdd)
    })
}

function removeNoteItem(divNoteItem) {
    divNoteItem.remove();
}

function getNoteItems() {
    var noteItems = []
    $('input[name="notelistitem"]').each(function() {
        noteItems.push($(this).val());
    });
    return noteItems;
}