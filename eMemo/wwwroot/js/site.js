﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
function initDatepicker() {
    $('.datepicker').datepicker({
        language: 'th-th',
        format: 'dd/mm/yyyy',
        autoclose: true
    });
}

function convertThaiToUS(dateThaiFormat) {
    var arr = dateThaiFormat.split("/");
    var year = parseInt(arr[2]) - 543;
    var newDate = arr[0] + "/" + arr[1] + "/" + year;
    return moment.utc(newDate, "DD/MM/YYYY").toDate();
}

function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

function getBase64andfile(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve({
            "fileBase64": reader.result,
            "fileName": file.name,
            "fileSize": file.size,
            "contentType": file.type
        });
        reader.onerror = error => reject(error);
    });
}


function buttonLoading(id, isLoading, defaultText) {
    if (isLoading) {
        $('#' + id + '').html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
         Loading...
        `);
    } else {
        if (defaultText == null) {
            defaultText = '';
        }
        $('#' + id + '').html(defaultText);
    }
    $('#' + id + '').prop("disabled", isLoading);
}

const validateEmail = (email) => {
    return String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
};

const validatePhoneNumber = (no) => {
    return String(no)
        .toLowerCase()
        .match(
            /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im
        );
};


async function mergeAllPDFs(urls) {
    const pdfDoc = await PDFLib.PDFDocument.create();
    const numDocs = urls.length;

    for (var i = 0; i < numDocs; i++) {
        const donorPdfBytes = await fetch(urls[i]).then(res => res.arrayBuffer());
        const donorPdfDoc = await PDFLib.PDFDocument.load(donorPdfBytes);
        const docLength = donorPdfDoc.getPageCount();
        for (var k = 0; k < docLength; k++) {
            const [donorPage] = await pdfDoc.copyPages(donorPdfDoc, [k]);
            //console.log("Doc " + i+ ", page " + k);
            pdfDoc.addPage(donorPage);
        }
    }
    const pdfDataUri = await pdfDoc.saveAsBase64({ dataUri: false });

    // strip off the first part to the first comma "data:image/png;base64,iVBORw0K..."
    //var data_pdf = pdfDataUri.substring(pdfDataUri.indexOf(',')+1);
    //console.log(pdfDataUri)
    printJS({ printable: pdfDataUri, type: 'pdf', base64: true })
}



async function createFile(filename, url) {
    let response = await fetch(url);
    let data = await response.blob();
    let metadata = {
        type: 'application/pdf'
    };
    let file = new File([data], filename, metadata);
    return file;
}